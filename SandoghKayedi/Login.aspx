﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="SandoghKayedi.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content=""/>
    <meta name="author" content="Mosaddek"/>
    <meta name="keyword" content="صندوق کایدی بختیاری"/>
    <link rel="shortcut icon" href="img/favicon.html"/>

    <title>صندوق خانوادگی کایدی بختیاری</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <link href="css/bootstrap-reset.css" rel="stylesheet"/>
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet"/>
    <link href="css/style-responsive.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        .auto-style1 {
            width: 48px;
            height: 48px;
        }
        .auto-style2 {
            width: 140px;
            height: 140px;
        }
    </style>
</head>

<body class="login-body" style="  background: linear-gradient(to bottom right , #fffa00,white); /* Standard syntax */">

    <div class="container" >
<map name="map">
    <area shape="rect" coords="0,50,165,90" href="Login.aspx" alt="kayedi"  />
</map>
      <form class="form-signin"  runat="server">
        <h2 class="form-signin-heading"> <img class="auto-style2" style="border-radius:20px;" src="images/logo.png" usemap="#map" />
            <br />
            <br />
<asp:Label runat="server" ForeColor="Black">صندوق خانوادگی کایدی بختیاری </asp:Label>     
                   <br />
           </h2>
        <div class="login-wrap" >

            
            <table style="margin:0px auto;width:100%">
                
                <tr>
                                        <td ><img class="auto-style1" src="img/User-blue-icon.png" />نام کاربری</td>
                    <td></td>
                        <td><img class="auto-style1" src="img/Register-icon.png" />رمز عبور</td>


                
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="user" runat="server" class="form-control" autofocus placeholder="نام کاربری" ForeColor="Black"></asp:TextBox></td>
                  <td style="width:10%"></td>
                     <td><asp:TextBox ID="pass" runat="server" type="password" class="form-control" placeholder="کلمه عبور" ForeColor="Black"></asp:TextBox></td>
                </tr>
                <tr style="text-align:center ">
                                        <td ><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="نام کاربری را وارد نمایید" ControlToValidate="user" Font-Names="B Nazanin" Font-Size="Medium" ForeColor="Red"></asp:RequiredFieldValidator></td>
                    <td style="width:10%"></td>
                     <td>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="رمز عبور را وارد نمایید" ControlToValidate="pass" Font-Names="B Nazanin" Font-Size="Medium" ForeColor="Red"></asp:RequiredFieldValidator>

                    </td>
                </tr>
            </table>
             

            
            <table style="margin:0px auto ">
                <tr>
                    <td>
            

                    </td>
                   
                </tr>
            </table>

               <img class="auto-style1" src="img/captcha.png" />لطفا متن درون عکس را وارد نمایید
            <br />
                                    <asp:Panel runat="server" DefaultButton="Button1">

            <table style="margin:0px auto " >
                <tr   >
                    <td  >
                        <asp:image ID="imagecaptcha"   AlternateText="Captcha"  runat="server" />
                    </td>
                    <td  >
                                        <asp:ImageButton ID="ImageButton1" CausesValidation="False" runat="server" ImageUrl="~/images/refresh-icon.png" OnClick="ImageButton1_Click" />

                    </td>
                </tr>
            <tr>
                <td colspan="2">
                                <p style="color:blue;font-size:12px;">*تصویر به بزرگی و کوچکی حروف حساس می باشد</p>

                </td>
            </tr>


            
                <tr style="text-align:center  " >
                    <td>
            <asp:TextBox ID="TextBox1" runat="server"  Class="form-control" placeholder="تصویر امنیتی" ForeColor="Black"></asp:TextBox>

                    </td>
                    
                    

                    
                </tr>
                <tr style="text-align:center ">
                    <td>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="کد امنیتی را وارد نمایید" ControlToValidate="TextBox1" Font-Names="B Nazanin" Font-Size="Medium" ForeColor="Red"></asp:RequiredFieldValidator>

                    </td>
                </tr>
            </table>
                            
            <p style="text-align:center;"> 
            <asp:Label ID="Label1" ForeColor="Red" runat="server" Visible="False" ></asp:Label>
            </p>
                </asp:Panel>

            <p style="margin:0px auto  ">
                
                            <asp:Button ID="Button1" Width="50%"  runat="server" class="btn btn-lg btn-login " Text="ورود" OnClick="Button1_Click" />

            </p>

            
        </div>

      </form>

    </div>
    <br /><br /><br />


  </body>
</html>
