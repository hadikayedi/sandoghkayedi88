﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SandoghKayedi
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        public static string data;
        public class Station
        {
            public bool Active { get; set; }
            public string Debbi { get; set; }
            public string EC { get; set; }
        }
       
        protected void Page_Load(object sender, EventArgs e)
        {
             data = String.Empty;
            WebClient wc = new WebClient();
            data = wc.DownloadString("http://localhost:32262/api/getinformation");

            Station r = JsonConvert.DeserializeObject<Station>(data);
            debbi.Text = r.Debbi;
            ec.Text = r.EC;
            active.Text = r.Active.ToString();
        }

        protected void timer_Tick(object sender, EventArgs e)
        {
            data = String.Empty;
            WebClient wc = new WebClient();
            data = wc.DownloadString("http://localhost:32262/api/getinformation");

            Station r = JsonConvert.DeserializeObject<Station>(data);
            debbi.Text = r.Debbi;
            ec.Text = r.EC;
            active.Text = r.Active.ToString();
        }
    }
}