﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ad.Master" AutoEventWireup="true" CodeBehind="CheckOut.aspx.cs" Inherits="SandoghKayedi.admin.CheckOut" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <script src="../script/PersianDatePicker.js"></script>
    <script src="../script/PersianDatePicker.min.js"></script>
    <link href="../Content/PersianDatePicker.min.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <style>
        
div.rcbSlide .RadComboBoxDropDown .rcbScroll
       {
 		font-family: 'B Yekan';
         font-size:14px;
    }

    </style>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
             <div class="col-lg-12"    >
                        <section class="panel">
                            <header class="panel-heading">
                                                  تسویه حساب عضو     
                            </header>
                                <div class="panel-body">
                                    <div class="input-group m-bot15" style="width:300px" >
                                <span class="input-group-addon">عضو مورد نظر</span>
                                        <telerik:RadComboBox CssClass="form-control blacktextbox" Font-Size="14px" Font-Names="B yekan" EmptyMessage="انتخاب نمایید" AllowCustomText="true" Filter="Contains" AutoPostBack="true" ID="Rad" runat="server" DataTextField="FLN" DataValueField="PersonalID" DataSourceID="DateSourceCheckOut" OnSelectedIndexChanged="Rad_SelectedIndexChanged"></telerik:RadComboBox>
                                        <asp:SqlDataSource runat="server" ID="DateSourceCheckOut" ConnectionString='<%$ ConnectionStrings:cnnstring %>' SelectCommand="SELECT *, [PersonalID], [ParentID], [Firstname]+' '+ [Lastname] as FLN, [Loanturn] FROM [tbl_Information] where Active=1 and ParentID is not null"></asp:SqlDataSource>
                                    </div>
                                    <telerik:RadGrid runat="server"  AlternatingItemStyle-Font-Names="B yekan" HeaderStyle-Font-Names="b yekan" ItemStyle-Font-Names="B yekan" CellSpacing="-1" DataSourceID="DateSourceInformationView" GridLines="Both" GroupPanelPosition="Top">
                                        <MasterTableView DataSourceID="DateSourceInformationView" AutoGenerateColumns="False">
                                            <Columns>
                                                <telerik:GridBoundColumn DataField="PersonalID" HeaderText="شناسه عضو" SortExpression="PersonalID" UniqueName="PersonalID" DataType="System.Int64" FilterControlAltText="Filter PersonalID column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="FLN" HeaderText="نام و نام خانوادگی عضو" SortExpression="FLN" UniqueName="FLN" FilterControlAltText="Filter FLN column" ReadOnly="True"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="Loanturn" HeaderText="نوبت وام" SortExpression="Loanturn" UniqueName="Loanturn" FilterControlAltText="Filter Loanturn column" DataType="System.Int64"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="ParentFLN" ReadOnly="True" HeaderText="نام و نام خانوادگی سرگروه" SortExpression="ParentFLN" UniqueName="ParentFLN" FilterControlAltText="Filter ParentFLN column"></telerik:GridBoundColumn>
                                                  <telerik:GridBoundColumn DataField="Fathername" HeaderText="نام پدر" SortExpression="Fathername" UniqueName="Fathername" FilterControlAltText="Filter Fathername column"></telerik:GridBoundColumn>
                                            </Columns>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                    <asp:SqlDataSource runat="server" ID="DateSourceInformationView" ConnectionString='<%$ ConnectionStrings:cnnstring %>' SelectCommand="SELECT * FROM [InformationView] WHERE ([PersonalID] = @PersonalID)">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="Rad" PropertyName="SelectedValue" Name="PersonalID" Type="Int64"></asp:ControlParameter>
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </div>
                            </section>
                 </div>
             <div class="col-lg-4"  runat="server" id="vampanel"   visible="false"    >
                        <section class="panel">
                            <header class="panel-heading">
                                                 وام های دریافتی     
                            </header>
                                <div class="panel-body">
                                    <telerik:RadGrid runat="server" AlternatingItemStyle-Font-Names="B yekan" HeaderStyle-Font-Names="b yekan" ItemStyle-Font-Names="B yekan" CellSpacing="-1" DataSourceID="DataSourceVam" GridLines="Both" GroupPanelPosition="Top">
                                        <MasterTableView DataSourceID="DataSourceVam" AutoGenerateColumns="False">
                                            <Columns>
                                                <telerik:GridBoundColumn DataField="marhale" ReadOnly="True" HeaderText="مرحله وام" SortExpression="marhale" UniqueName="marhale" FilterControlAltText="Filter marhale column"></telerik:GridBoundColumn>

                                                <telerik:GridBoundColumn DataField="vam" HeaderText="مبلغ وام" SortExpression="vam" UniqueName="vam" FilterControlAltText="Filter vam column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="month" ReadOnly="True" HeaderText="ماه" SortExpression="month" UniqueName="month" FilterControlAltText="Filter month column"></telerik:GridBoundColumn>

                                                  <telerik:GridBoundColumn DataField="year" ReadOnly="True" HeaderText="سال" SortExpression="year" UniqueName="year" FilterControlAltText="Filter year column"></telerik:GridBoundColumn>
                                            </Columns>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                    <asp:SqlDataSource runat="server" ID="DataSourceVam" ConnectionString='<%$ ConnectionStrings:cnnstring %>' SelectCommand="SELECT * FROM [VamCheckOut] WHERE ([ssid] = @ssid)">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="Rad" PropertyName="SelectedValue" Name="ssid" Type="String"></asp:ControlParameter>
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </div>
                            </section>
                 </div>
            <div class="col-lg-2" runat="server" id="pardakhtipanel"  visible="false"     >
                        <section class="panel">
                            <header class="panel-heading">
                                                 مجموع پرداختی ها     
                            </header>
                                <div class="panel-body">
                                    <telerik:RadGrid runat="server" AlternatingItemStyle-Font-Names="B yekan" HeaderStyle-Font-Names="b yekan" ItemStyle-Font-Names="B yekan" CellSpacing="-1" DataSourceID="DataSourcePardakhti" GridLines="Both" GroupPanelPosition="Top">
                                        <MasterTableView DataSourceID="DataSourcePardakhti" AutoGenerateColumns="False">
                                            <Columns>
                                                <telerik:GridBoundColumn DataField="idvam" HeaderText="دوره" SortExpression="idvam" UniqueName="idvam" FilterControlAltText="Filter idvam column"></telerik:GridBoundColumn>

                                                <telerik:GridBoundColumn DataField="sumpardakhti" HeaderText="مجموع بازپرداخت ها" SortExpression="sumpardakhti" UniqueName="sumpardakhti" DataType="System.Double" FilterControlAltText="Filter sumpardakhti column"></telerik:GridBoundColumn>

                                                  </Columns>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                    <asp:SqlDataSource runat="server" ID="DataSourcePardakhti" ConnectionString='<%$ ConnectionStrings:cnnstring %>' SelectCommand="SELECT * FROM [PardakhtiCheckOut] WHERE ([ssid] = @ssid)">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="Rad" PropertyName="SelectedValue" Name="ssid" Type="String"></asp:ControlParameter>
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </div>
                            </section>
                 </div>
            <div class="col-lg-2" runat="server" id="ozviatpanel"  visible="false"     >
                        <section class="panel">
                            <header class="panel-heading">
                                                 مجموع حق عضویت ها     
                            </header>
                                <div class="panel-body">
                                    <telerik:RadGrid runat="server" AlternatingItemStyle-Font-Names="B yekan" HeaderStyle-Font-Names="b yekan" ItemStyle-Font-Names="B yekan" CellSpacing="-1" DataSourceID="SqlDataSource1" GridLines="Both" GroupPanelPosition="Top">
                                        <MasterTableView DataSourceID="SqlDataSource1" AutoGenerateColumns="False">
                                            <Columns>
                                                <telerik:GridBoundColumn DataField="marhale" HeaderText="دوره" SortExpression="marhale" UniqueName="marhale" FilterControlAltText="Filter marhale column"></telerik:GridBoundColumn>

                                                <telerik:GridBoundColumn DataField="ozviat" HeaderText="مجموع حق عضویت ها ها" SortExpression="ozviat" UniqueName="ozviat" DataType="System.Double" FilterControlAltText="Filter ozviat column"></telerik:GridBoundColumn>

                                                  </Columns>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:cnnstring %>' SelectCommand="SELECT * FROM [OzviatCheckOut] WHERE ([ssid] = @ssid)">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="Rad" PropertyName="SelectedValue" Name="ssid" Type="String"></asp:ControlParameter>
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </div>
                            </section>
                 </div>
            <div class="col-lg-4"   runat="server" id="Abstractpanel"  visible="false"  >
                        <section class="panel">
                            <header class="panel-heading">
                                                 خلاصه وضعیت     
                            </header>
                                <div class="panel-body">
                                    <div class="input-group m-bot15" style="width:350px" >
                                <span class="input-group-addon">آخرین وام دریافتی</span>
            <asp:TextBox ReadOnly="true" BackColor="White" ID="LastVam" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
                 </div>
                                    <div class="input-group m-bot15" style="width:350px" >
                                <span class="input-group-addon">مجموع بازپرداخت آخرین دوره</span>
            <asp:TextBox ReadOnly="true" BackColor="White" ID="Sumbazpardakht" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
                 </div>
                                     <div class="input-group m-bot15" style="width:350px" >
                                <span class="input-group-addon">مبلغ مانده از وام</span>
            <asp:TextBox ReadOnly="true" BackColor="White" ID="MandeVam" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
                 </div>
                                    <div class="input-group m-bot15" style="width:350px" >
                                <span class="input-group-addon">مجموع حق عضویت ها</span>
            <asp:TextBox ReadOnly="true" BackColor="White" ID="Sumozviat" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
                 </div>
                                    <div class="input-group m-bot15" style="width:350px" >
                                <span class="input-group-addon">مانده کل</span>
            <asp:TextBox ReadOnly="true" BackColor="White" ID="Mande" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
                 </div>
                                     <div class="input-group m-bot15" style="width:350px" >
                                <span class="input-group-addon">آخرین ماه ثبت شده</span>
            <asp:TextBox ReadOnly="true" BackColor="White" ID="lastdate" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
                 </div>
                                    <div class="input-group m-bot15" style="width:350px" >
                                <span class="input-group-addon">وضعیت</span>
            <asp:TextBox ReadOnly="true" BackColor="White" ID="Status" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
                 </div>
                                   
                                    <div class="input-group m-bot15" style="width:350px" >
                                <span class="input-group-addon">شماره فیش</span>
                               <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="fish" ForeColor="Red" ValidationGroup="c" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>

            <asp:TextBox  BackColor="White" ID="fish" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
                 </div>
                                    <div class="input-group m-bot15" style="width:350px" >
                                <span class="input-group-addon">تاریخ فیش</span>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="date" ForeColor="Red" ValidationGroup="c" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
            <asp:TextBox  BackColor="White" ID="date" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
                                         
                 </div>
                                    <div class="input-group m-bot15" style="width:350px" >
                                <span class="input-group-addon">توضیحات</span>
            <asp:TextBox TextMode="MultiLine"  BackColor="White" ID="Tozihat" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
                                        <br />
                                         
                 </div>
 <asp:Label runat="server" ID="messagebox"></asp:Label>
                                    <br /><br />
               <asp:Button ID="CheckOutButton" runat="server" CssClass="btn btn-info" Text="تسویه حساب" ValidationGroup="c" OnClick="CheckOutButton_Click"   />

                                    </div>
                            </section>
                 </div>
            
            </ContentTemplate>
            </asp:UpdatePanel>
</asp:Content>
