﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SandoghKayedi.admin
{
    public partial class VamSubmit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["type"]==null || Session["type"].ToString() != "Admin")
            {
                Response.Redirect("/Login.aspx");
            }
            PersianDateTime now = PersianDateTime.Now;
            string persianDate = now.ToString(PersianDateTimeFormat.Date);
            string s = persianDate;
            datepicker1.Attributes["onclick"] = "PersianDatePicker.Show(this, '" + s + "');";
        }

        protected string GetParentID()
        {
            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "GetParentID";






          
                   
                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add("@UserID", SqlDbType.NVarChar).Value =User.SelectedValue.ToString();
                    conn.Open();
                    cmd.ExecuteNonQuery();
            string res=returnParameter.Value.ToString();
            conn.Close();
            return res;
           
                

           
        }
        protected void btnnew_Click(object sender, EventArgs e)
        {
            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "insert into vam values('"+User.SelectedValue.ToString()+"','"+GetParentID()+"',NULL,NULL,'"+vam.SelectedItem.Text.Replace(",","")+"',NULL,NULL,'"+ComboYear.SelectedValue.ToString()+ComboMonth.SelectedValue.ToString()+"','"+RadComboBox2.SelectedItem.Text+"','"+Paytype.SelectedItem.Text+"','"+fish.Text+"',NULL,NULL,NULL,'"+datepicker1.Text+"','"+karmozd.Text+"','"+desc.Text+"')";

            conn.Open();
            cmd.ExecuteNonQuery();
            GreenMessagebox.Visible = true;
            conn.Close();


        }

        protected void SubmitNewFish_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }
    }
}