﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ad.Master" AutoEventWireup="true" CodeBehind="LoanTurns.aspx.cs" Inherits="SandoghKayedi.admin.LoanTurns" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" ChildrenAsTriggers="true" runat="server">

        <ContentTemplate>

            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        افزودن عضو و خانواده جدید     
                    </header>
                    <div class="panel-body">
                        <telerik:RadGrid runat="server"  AlternatingItemStyle-Font-Names="B yekan" HeaderStyle-Font-Names="b yekan" ItemStyle-Font-Names="B yekan" CellSpacing="-1" DataSourceID="SqlDataSource2" GridLines="Both" GroupPanelPosition="Top" ID="Radgrid"  >
                                        <MasterTableView DataSourceID="SqlDataSource2" AutoGenerateColumns="False">
                                            <Columns>
                                                <telerik:GridBoundColumn DataField="PersonalID" HeaderText="شماره عضویت" SortExpression="PersonalID" UniqueName="PersonalID" DataType="System.Int64" FilterControlAltText="Filter PersonalID column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="FLN" HeaderText="نام و نام خانوادگی" SortExpression="FLN" UniqueName="FLN" FilterControlAltText="Filter FLN column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="Loanturn" HeaderText="نوبت وام" SortExpression="Loanturn" UniqueName="Loanturn" DataType="System.Int64" FilterControlAltText="Filter Loanturn column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="SumOzviat" HeaderText="مجموع حق عضویت" SortExpression="SumOzviat" UniqueName="SumOzviat" DataType="System.Int64" FilterControlAltText="Filter SumOzviat column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="SumVam" HeaderText="مجموع وام دریافتی" SortExpression="SumVam" UniqueName="SumVam" DataType="System.Int64" FilterControlAltText="Filter SumVam column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="Active" HeaderText="فعال" SortExpression="Active" UniqueName="Active" DataType="System.Int64" FilterControlAltText="Filter Active column"></telerik:GridBoundColumn>
                                                                                                                        

                                            </Columns>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                         <asp:SqlDataSource runat="server" ID="SqlDataSource2" ConnectionString='<%$ ConnectionStrings:cnnstring %>' ProviderName="System.Data.SqlClient" SelectCommand="SELECT *,Firstname+' '+Lastname as FLN FROM [InformationWithVamPardakhti]  where PersonalID>0 and Active='فعال' order by Loanturn asc "  ></asp:SqlDataSource>
                        </div>
                    </section>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
</asp:Content>
