﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace SandoghKayedi.admin
{
    public partial class Dates : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void RadGrid1_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                GridDataItem data = (GridDataItem)e.Item;
                IDtemp.Text = data["Id"].Text == "&nbsp;" ? "" : data["Id"].Text;
                marhaletemp.Text = data["marhale"].Text == "&nbsp;" ? "" : data["marhale"].Text;
                frtemp.Text = data["fr"].Text == "&nbsp;" ? "" : data["fr"].Text;
                ttemp.Text = data["t"].Text == "&nbsp;" ? "" : data["t"].Text;
                antacktemp.Text = data["antrack"].Text == "&nbsp;" ? "" : data["antrack"].Text;
                haghozviattemp.Text = data["haghozviat"].Text == "&nbsp;" ? "" : data["haghozviat"].Text;
                idvamtemp.Text = data["idvam"].Text == "&nbsp;" ? "" : data["idvam"].Text;
                vamtemp.Text = data["vam"].Text == "&nbsp;" ? "" : data["vam"].Text;
                tedadtemp.Text = data["tedad"].Text == "&nbsp;" ? "" : data["tedad"].Text;
              

                divEdit.Visible = true;
            }
        }

        protected void updateDates()
        {

            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString;
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "update payenew set marhale=@marhale,fr=@fr,t=@t,antrack=@antrack,haghozviat=@haghozviat,"
                                          +"idvam=@idvam,vam=@vam,tedad=@tedad where Id=@id";
                    command.Parameters.AddWithValue("id", IDtemp.Text);
                    command.Parameters.AddWithValue("marhale", marhaletemp.Text);
                    command.Parameters.AddWithValue("fr", frtemp.Text);
                    command.Parameters.AddWithValue("t", ttemp.Text);
                    command.Parameters.AddWithValue("antrack", antacktemp.Text);
                    command.Parameters.AddWithValue("haghozviat", haghozviattemp.Text);
                    command.Parameters.AddWithValue("idvam", idvamtemp.Text);
                    command.Parameters.AddWithValue("vam", vamtemp.Text);
                    command.Parameters.AddWithValue("tedad", tedadtemp.Text);
                    
                    connection.Open();

                    command.ExecuteNonQuery();
                }
            }

        }
        protected void editDates_Click(object sender, EventArgs e)
        {
            updateDates();
            divEdit.Visible = false;
            RadGrid1.DataBind();
        }
    }
}