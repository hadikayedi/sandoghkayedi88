﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SandoghKayedi.admin
{
    public partial class NewUserSubmit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["type"]==null || Session["type"].ToString() != "Admin")
            {
                Response.Redirect("/Login.aspx");
            }
            PersianDateTime now = PersianDateTime.Now;
            string persianDate = now.ToString(PersianDateTimeFormat.Date);
            string s = persianDate;
            datepicker.Attributes["onclick"] = "PersianDatePicker.Show(this, '" + s + "');";
        }

        protected void CheckBoxList1_SelectedIndexChanged(object sender, EventArgs e)
        {
             var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "GetPayeOzviat";





            int mablaghkol=0;
            foreach (ListItem item in CheckBoxList1.Items)
                if (item.Selected)
                {
                    cmd.Parameters.Clear();
                    SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                    returnParameter.Direction = ParameterDirection.ReturnValue;
cmd.Parameters.Add("@date", SqlDbType.NVarChar).Value = item.Value.ToString();
                    conn.Open();
            cmd.ExecuteNonQuery();
           

                    mablaghkol = mablaghkol + int.Parse(returnParameter.Value.ToString());
                    conn.Close();
                }

            hadi.Text = mablaghkol.ToString();
            double amount1 = Convert.ToDouble(hadi.Text.Trim());
            hadi.Text = amount1.ToString("#,##0");
        }

        protected void btnnew_Click(object sender, EventArgs e)
        {
            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);
            
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;


            foreach (ListItem item in CheckBoxList1.Items)
                if (item.Selected)
                {
                    cmd.Parameters.Clear();
                    cmd.CommandText = "SubmitPardakhtiNafar";
                    cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = parent.SelectedValue.ToString();
                    cmd.Parameters.Add("@date", SqlDbType.NVarChar).Value = item.Value;
                    cmd.Parameters.Add("@PayType", SqlDbType.NVarChar).Value = Paytype.SelectedValue.ToString();
                    cmd.Parameters.Add("@Fishdate", SqlDbType.NVarChar).Value = datepicker.Text;
                    cmd.Parameters.Add("@fish", SqlDbType.NVarChar).Value = fish.Text;
                    cmd.Parameters.Add("@tozihat", SqlDbType.NVarChar).Value = desc.Text;

                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }

            var script =
   "Sys.WebForms.PageRequestManager.getInstance()._scrollPosition = null; " +
   "window.scrollTo(0, 0);";

            ScriptManager.RegisterStartupScript(this, GetType(), "key", script, true);
            GetInformationNewUser();
            CheckBoxList1.ClearSelection();
            hadi.Text = "0";
            Paytype.SelectedIndex = 0;
            datepicker.Text = "";
            desc.Text = "";
            fish.Text = "";
            CheckBoxList1.DataBind();
            




        }
        protected string GetLastMonth(string UserID)
        {

            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;


            cmd.CommandText = "GetLastMonthYearNafar";
            cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;




            string month;
            conn.Open();
            SqlDataReader dr;
            dr = cmd.ExecuteReader();


            dr.Read();
            month = dr["Lastmonthfarsi"].ToString();
            conn.Close();
            return month;









        }
        protected string GetLastYear(string UserID)
        {

            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;


            cmd.CommandText = "GetLastMonthYearNafar";
            cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
  



            string year;
            conn.Open();
            SqlDataReader dr;
            dr = cmd.ExecuteReader();


            dr.Read();
            year = dr["Lastyear"].ToString();
            conn.Close();
            return year;









        }
        protected string GetSumPardakhti(string ssid)
        {

            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;


            cmd.CommandText = "GetSumOzviat";
            cmd.Parameters.Add("@ssid", SqlDbType.Int).Value = ssid;
            



            string Sum;
            conn.Open();
            SqlDataReader dr;
            dr = cmd.ExecuteReader();


            dr.Read();
            Sum = dr["SumOzviat"].ToString();
            conn.Close();
            return Sum;









        }
        protected void GetInformationNewUser()
        {
            if (GetLastYear(parent.SelectedValue.ToString()) != "")
                monthyear.Text = GetLastMonth(parent.SelectedValue.ToString()) + ' ' + GetLastYear(parent.SelectedValue.ToString());
            else
                monthyear.Text = "تا کنون مبلغی در سیستم ثبت نشده است";

            string sum=GetSumPardakhti(parent.SelectedValue.ToString());
            if (sum != "")
                SumPardakhti.Text = GetSumPardakhti(parent.SelectedValue.ToString());
            else
                SumPardakhti.Text = "0";

            double amount = Convert.ToDouble(SumPardakhti.Text.Trim());
            SumPardakhti.Text = amount.ToString("#,##0");

        }

        protected void parent_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            GetInformationNewUser();

        }


    }
}