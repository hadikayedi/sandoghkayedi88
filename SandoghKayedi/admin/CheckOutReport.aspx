﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ad.Master" AutoEventWireup="true" CodeBehind="CheckOutReport.aspx.cs" Inherits="SandoghKayedi.admin.CheckOutReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        
.dir
{
  direction:ltr ;
  text-align:center;
}

    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
     <div class="col-lg-12"   >
                        <section class="panel">
                            <header class="panel-heading">
                                                  تسویه حساب ها     
                            </header>
                                <div class="panel-body">
    <telerik:RadGrid AlternatingItemStyle-Font-Names="B yekan" ItemStyle-HorizontalAlign="Center" AlternatingItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Font-Names="b yekan" ItemStyle-Font-Names="B yekan" runat="server" CellSpacing="-1" DataSourceID="CheckOut" GridLines="Both" GroupPanelPosition="Top">
        <MasterTableView DataSourceID="CheckOut" AutoGenerateColumns="False">
            <Columns>
                <telerik:GridBoundColumn DataField="Firstname" HeaderText="نام" SortExpression="Firstname" UniqueName="Firstname" FilterControlAltText="Filter Firstname column"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Lastname" HeaderText="نام خانوادگی" SortExpression="Lastname" UniqueName="Lastname" FilterControlAltText="Filter Lastname column"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Loanturn" HeaderText="نوبت وام" SortExpression="Loanturn" UniqueName="Loanturn" DataType="System.Int64" FilterControlAltText="Filter Loanturn column"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Lastvam" HeaderText="آخرین وام دریافتی" SortExpression="Lastvam" UniqueName="Lastvam" FilterControlAltText="Filter Lastvam column"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Bazpardakht" HeaderText="مجموع بازپرداخت ها" SortExpression="Bazpardakht" UniqueName="Bazpardakht" FilterControlAltText="Filter Bazpardakht column"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Remainvam" HeaderText="باقیمانده وام" SortExpression="Remainvam" UniqueName="Remainvam" FilterControlAltText="Filter Remainvam column"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Sumozviat" HeaderText="مجموع حق عضویت ها" SortExpression="Sumozviat" UniqueName="Sumozviat" FilterControlAltText="Filter Sumozviat column"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Remain" ItemStyle-CssClass="dir" HeaderText="باقیمانده" SortExpression="Remain" UniqueName="Remain" FilterControlAltText="Filter Remain column"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Status" HeaderText="وضعیت" SortExpression="Status" UniqueName="Status" FilterControlAltText="Filter Status column"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Fish" HeaderText="شماره فیش" SortExpression="Fish" UniqueName="Fish" FilterControlAltText="Filter Fish column"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Fishdate" HeaderText="تاریخ فیش" SortExpression="Fishdate" UniqueName="Fishdate" FilterControlAltText="Filter Fishdate column"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Submitdate" HeaderText="تاریخ ثبت تسویه حساب" SortExpression="Submitdate" UniqueName="Submitdate" FilterControlAltText="Filter Submitdate column"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Tozihat" HeaderText="توضیحات" SortExpression="Tozihat" UniqueName="Tozihat" FilterControlAltText="Filter Tozihat column"></telerik:GridBoundColumn>

                 </Columns>
        </MasterTableView>
    </telerik:RadGrid>
                                    </div>
                            </section>
         </div>
    <asp:SqlDataSource runat="server" ID="CheckOut" ConnectionString='<%$ ConnectionStrings:cnnstring %>' SelectCommand="SELECT * FROM [CheckOutView]"></asp:SqlDataSource>
</asp:Content>
