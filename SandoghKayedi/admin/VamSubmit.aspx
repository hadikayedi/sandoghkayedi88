﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ad.Master" AutoEventWireup="true" CodeBehind="VamSubmit.aspx.cs" Inherits="SandoghKayedi.admin.VamSubmit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <script src="../script/PersianDatePicker.js"></script>
    <script src="../script/PersianDatePicker.min.js"></script>
    <link href="../Content/PersianDatePicker.min.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <style>
        
div.rcbSlide .RadComboBoxDropDown .rcbScroll
       {
 		font-family: 'B Yekan';
         font-size:14px;
    }

        .Treeviewheader{
            font-family:"B Yekan";
            font-size:14px;
            font-weight:bold;

        }
        .Treeviewitem{
            font-family:"B Yekan";
            font-size:13px;

        }
        .Treeviewalter{
            font-family:"B Yekan";
            font-size:13px;


        }
        div.rcbSlide .RadComboBoxDropDown .rcbScroll
       {
 		font-family: 'B Yekan';
         font-size:14px;
    }


    </style>
    <asp:ScriptManager runat="server"></asp:ScriptManager>
      <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
             <div class="col-lg-4"    >
                        <section class="panel">
                            <header class="panel-heading">
                                                  ثبت وام      
                            </header>
                                <div class="panel-body">


            <div class="input-group m-bot15" style="width:300px" >
                                <span class="input-group-addon">فرد مورد نظر</span>
                <telerik:RadComboBox runat="server" CssClass="form-control blacktextbox" Font-Size="14px" Font-Names="B yekan" EmptyMessage="انتخاب نمایید" AllowCustomText="true" Filter="Contains" AutoPostBack="true" ID="User" DataSourceID="SqlDataSource1" DataTextField="FLN" DataValueField="PersonalID"></telerik:RadComboBox>


                <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:cnnstring %>' ProviderName="System.Data.SqlClient" SelectCommand="SELECT *,Firstname+' '+Lastname as FLN FROM [tbl_Information] where PersonalID > 0"></asp:SqlDataSource>
            </div>
                                    <div class="input-group m-bot15" style="width:400px" >
                                <span class="input-group-addon">مبلغ وام (ریال)</span>
                                        <telerik:RadComboBox CssClass="form-control blacktextbox" EmptyMessage="انتخاب نمایید" AllowCustomText="true" Filter="Contains" AutoPostBack="true" Font-Size="14px" Font-Names="B yekan" ID="vam" runat="server" DataSourceID="SqlDataSource2" DataTextField="vam" DataValueField="idvam">
                                        </telerik:RadComboBox>
                                         <telerik:RadComboBox CssClass="form-control blacktextbox" Enabled="false" ForeColor="black" BackColor="White" Font-Size="14px" Font-Names="B yekan" ID="RadComboBox2" runat="server" DataSourceID="SqlDataSource3" DataTextField="idvam" DataValueField="vam">
                                        </telerik:RadComboBox>
                                        <asp:SqlDataSource runat="server" ID="SqlDataSource2" ConnectionString='<%$ ConnectionStrings:cnnstring %>' ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM [PayeNewView] where idvam>0"></asp:SqlDataSource>
                                        <asp:SqlDataSource runat="server" ID="SqlDataSource3" ConnectionString='<%$ ConnectionStrings:cnnstring %>' ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM [PayeNewView] WHERE ([idvam] = @idvam)">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="vam" PropertyName="SelectedValue" Name="idvam" Type="String"></asp:ControlParameter>
                                            </SelectParameters>
                                        </asp:SqlDataSource>

                                                    </div>
                                    <div class="input-group m-bot15" style="width:350px" >
                                <span class="input-group-addon">ماه و سال</span>
                                        <telerik:RadComboBox  BackColor="White" CssClass="form-control blacktextbox" ForeColor="Black" Font-Size="14px"  Font-Names="B yekan"   ID="ComboMonth" runat="server"  >
                    <Items>
                        <telerik:RadComboBoxItem runat="server" Text="فروردین" Value="01"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="اردیبهشت" Value="02"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="خرداد" Value="03"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="تیر" Value="04"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="مرداد" Value="05"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="شهریور" Value="06"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="مهر" Value="07"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="آبان" Value="08"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="آذر" Value="09"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="دی" Value="10"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="بهمن" Value="11"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="اسفند" Value="12"></telerik:RadComboBoxItem>
                    </Items>
                </telerik:RadComboBox>
                                         <telerik:RadComboBox   BackColor="White" CssClass="form-control blacktextbox" ForeColor="Black" Font-Size="14px"  Font-Names="B yekan"   ID="ComboYear" runat="server"  >
                    <Items>
                        <telerik:RadComboBoxItem runat="server" Text="1395" Value="1395"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="1396" Value="1396"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="1397" Value="1397"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="1398" Value="1398"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="1399" Value="1399"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="1400" Value="1400"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="1401" Value="1401"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="1402" Value="1402"></telerik:RadComboBoxItem>
         
                    </Items>
                </telerik:RadComboBox>
                                    </div>
                          

                                    <div class="input-group m-bot15" style="width:250px" >
                                <span class="input-group-addon">نوع پرداخت</span>
                <telerik:RadComboBox  CssClass="form-control blacktextbox" Font-Size="14px"  Font-Names="B yekan"     ID="Paytype" runat="server"  >
                    <Items>
                        <telerik:RadComboBoxItem runat="server" Text="پرداخت اینترنتی" Value="پرداخت اینترنتی"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="پرداخت نقدی" Value="پرداخت نقدی"></telerik:RadComboBoxItem>
                    </Items>
                </telerik:RadComboBox>
                 </div>
                                    <div class="input-group m-bot15" style="width:250px" >
                                <span class="input-group-addon">تاریخ</span>
                                         
            <asp:TextBox  ID="datepicker1" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
                                       
                 </div>

                                    <div class="input-group m-bot15" style="width:250px" >
                                <span class="input-group-addon">شماره فیش</span>
            <asp:TextBox  ID="fish" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>

                 </div>
                                      <div class="input-group m-bot15" style="width:250px" >
                                <span class="input-group-addon">کارمزد (ریال)</span>
            <asp:TextBox  ID="karmozd" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>

                 </div>
                                    <div class="input-group m-bot15" style="width:250px" >
                                <span class="input-group-addon">توضیحات</span>
            <asp:TextBox  ID="desc" TextMode="MultiLine" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan"  runat="server"  ></asp:TextBox>
                 </div>

                            <asp:Button Text="ثبت" CssClass="btn btn-info"   runat="server" ID="btnnew" OnClick="btnnew_Click"/>

                                      <br />

                                    <div runat="server" id="GreenMessagebox" visible="false" style="background-color:lightgreen;margin-top:20px;width:300px;margin:0px auto;height:100px;text-align:center;border-radius:20px">
                                        وام مورد نظر با موفقیت ثبت شد
                                        <br />
                                        <br />
                                        <br />

                                <asp:Button ID="SubmitNewFish" runat="server" CssClass="btn btn-info" Text="ثبت وام جدید" OnClick="SubmitNewFish_Click"  />


                </div>

                                    </section>
                 </div>
            <div class="col-lg-8"    >
                        <section class="panel">
                            <header class="panel-heading">
                                                  وام ها      
                            </header>
                                <div class="panel-body">
                                    <telerik:RadGrid runat="server" AlternatingItemStyle-Font-Names="B yekan" HeaderStyle-Font-Names="b yekan" ItemStyle-Font-Names="B yekan" CellSpacing="-1" DataSourceID="SqlDataSource4" GridLines="Both" GroupPanelPosition="Top">
                                        <MasterTableView DataSourceID="SqlDataSource4" AutoGenerateColumns="False">
                                            <Columns>
                                                <telerik:GridBoundColumn DataField="ssid" HeaderText="شماره عضویت" SortExpression="ssid" UniqueName="ssid" FilterControlAltText="Filter ssid column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="vam" HeaderText="مبلغ وام" SortExpression="vam" UniqueName="vam" FilterControlAltText="Filter vam column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="month" HeaderText="ماه" SortExpression="month" UniqueName="month" FilterControlAltText="Filter month column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="year" HeaderText="سال" SortExpression="year" UniqueName="year" DataType="System.Int64" FilterControlAltText="Filter year column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="marhale" HeaderText="مرحله" SortExpression="marhale" UniqueName="marhale" FilterControlAltText="Filter marhale column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="fish" HeaderText="شماره فیش" SortExpression="fish" UniqueName="fish" FilterControlAltText="Filter fish column"></telerik:GridBoundColumn>


                                                <telerik:GridBoundColumn DataField="tozihat" HeaderText="توضیحات" SortExpression="tozihat" UniqueName="tozihat" FilterControlAltText="Filter tozihat column"></telerik:GridBoundColumn>
                                            </Columns>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                    <asp:SqlDataSource runat="server" ID="SqlDataSource4" ConnectionString='<%$ ConnectionStrings:cnnstring %>' ProviderName="System.Data.SqlClient" SelectCommand="SELECT [ssid], [vam], [month], [year], [marhale], [fish], [tozihat] FROM [vam] WHERE ([ssid] = @ssid)">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="User" PropertyName="SelectedValue" Name="ssid" Type="String"></asp:ControlParameter>
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                    </div>
                            </section>
                </div>
            </ContentTemplate>
          </asp:UpdatePanel>
</asp:Content>
