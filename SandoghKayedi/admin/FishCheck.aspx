﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ad.Master" AutoEventWireup="true" CodeBehind="FishCheck.aspx.cs" Inherits="SandoghKayedi.admin.FishCheck" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <style>
        .Treeviewheader{
            font-family:"B Yekan";
            font-size:14px;
            font-weight:bold;

        }
        .Treeviewitem{
            font-family:"B Yekan";
            font-size:13px;

        }
        .Treeviewalter{
            font-family:"B Yekan";
            font-size:13px;


        }
        div.rcbSlide .RadComboBoxDropDown .rcbScroll
       {
 		font-family: 'B Yekan';
         font-size:14px;
    }
    </style>
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
    <ContentTemplate>
        <div class="col-lg-12" id="divselect" visible="true"  runat="server"  >
                        <section class="panel">
                            <header class="panel-heading">
جست و جو
                            </header>
                             <div class="input-group m-bot15" style="width:300px" >
                                <span class="input-group-addon">
                                    فیش
                                </span>
                                 <telerik:RadComboBox runat="server" EnableVirtualScrolling="true" DropDownHeight="20px" CssClass="form-control blacktextbox"  Font-Size="14px" Font-Names="B yekan" EmptyMessage="انتخاب نمایید" AllowCustomText="true" Filter="Contains" AutoPostBack="true" ID="fish" DataSourceID="SqlDataSource1" DataTextField="fish" DataValueField="fish"></telerik:RadComboBox>
                                 <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:cnnstring %>' ProviderName="System.Data.SqlClient" SelectCommand="SELECT [fish] FROM [Fishes]"></asp:SqlDataSource>
                             </div>

                            </section>
            </div>
         <div class="col-lg-4" id="div1" visible="true"  runat="server"  >
                        <section class="panel">
                            <header class="panel-heading">
پرداختی
                            </header>
                            <telerik:RadGrid runat="server" HeaderStyle-CssClass="Treeviewheader" ItemStyle-CssClass="Treeviewitem" AlternatingItemStyle-CssClass="Treeviewalter" CellSpacing="-1" DataSourceID="SqlDataSource2" GridLines="Both" GroupPanelPosition="Top">
                                <MasterTableView DataSourceID="SqlDataSource2" AutoGenerateColumns="False">
                                      <NoRecordsTemplate >
                                       <center>
                                           موردی جهت نمایش یافت نشد
                                       </center>
                                        
                                    </NoRecordsTemplate>
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="Firstname" HeaderText="Firstname" SortExpression="Firstname" UniqueName="Firstname" FilterControlAltText="Filter Firstname column"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Lastname" HeaderText="Lastname" SortExpression="Lastname" UniqueName="Lastname" FilterControlAltText="Filter Lastname column"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="fish" HeaderText="fish" SortExpression="fish" UniqueName="fish" FilterControlAltText="Filter fish column"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="FishDate" HeaderText="FishDate" SortExpression="FishDate" UniqueName="FishDate" FilterControlAltText="Filter FishDate column"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="typepay" HeaderText="typepay" SortExpression="typepay" UniqueName="typepay" FilterControlAltText="Filter typepay column"></telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                            </telerik:RadGrid>
                            <asp:SqlDataSource runat="server" ID="SqlDataSource2" ConnectionString='<%$ ConnectionStrings:cnnstring %>' ProviderName="System.Data.SqlClient" SelectCommand="SELECT [Firstname], [Lastname], [fish], [FishDate], [typepay] FROM [PardakhtiWithNames] WHERE ([fish] = @fish)">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="fish" PropertyName="SelectedValue" Name="fish" Type="String"></asp:ControlParameter>
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </section>
            </div>
         <div class="col-lg-4" id="div2" visible="true"  runat="server"  >
                        <section class="panel">
                            <header class="panel-heading">
وام
                            </header>
                            <telerik:RadGrid runat="server" HeaderStyle-CssClass="Treeviewheader" ItemStyle-CssClass="Treeviewitem" AlternatingItemStyle-CssClass="Treeviewalter" CellSpacing="-1" DataSourceID="SqlDataSource3" GridLines="Both" GroupPanelPosition="Top">
                                <MasterTableView DataSourceID="SqlDataSource3" AutoGenerateColumns="False">
                                      <NoRecordsTemplate >
                                       <center>
                                           موردی جهت نمایش یافت نشد
                                       </center>
                                        
                                    </NoRecordsTemplate>
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="Firstname" HeaderText="Firstname" SortExpression="Firstname" UniqueName="Firstname" FilterControlAltText="Filter Firstname column"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Lastname" HeaderText="Lastname" SortExpression="Lastname" UniqueName="Lastname" FilterControlAltText="Filter Lastname column"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Fishdate" HeaderText="Fishdate" SortExpression="Fishdate" UniqueName="Fishdate" FilterControlAltText="Filter Fishdate column"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="fish" HeaderText="fish" SortExpression="fish" UniqueName="fish" FilterControlAltText="Filter fish column"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="typepay" HeaderText="typepay" SortExpression="typepay" UniqueName="typepay" FilterControlAltText="Filter typepay column"></telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                            </telerik:RadGrid>
                            <asp:SqlDataSource runat="server" ID="SqlDataSource3" ConnectionString='<%$ ConnectionStrings:cnnstring %>' ProviderName="System.Data.SqlClient" SelectCommand="SELECT [Firstname], [Lastname], [Fishdate], [fish], [typepay] FROM [VamWithNames] WHERE ([fish] = @fish)">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="fish" PropertyName="SelectedValue" Name="fish" Type="String"></asp:ControlParameter>
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </section>
            </div>
         <div class="col-lg-4" id="div3" visible="true"  runat="server"  >
                        <section class="panel">
                            <header class="panel-heading">
تسویه حساب
                            </header>
                            </section>
            </div>
                            </ContentTemplate>
        </asp:UpdatePanel>
</asp:Content>
