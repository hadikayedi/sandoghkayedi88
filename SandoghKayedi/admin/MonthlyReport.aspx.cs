﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace SandoghKayedi.admin
{
    public partial class MonthlyReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["type"]==null || Session["type"].ToString() != "Admin")
            {
                Response.Redirect("/Login.aspx");
            }
        }

        protected void btnExportexcel_Click(object sender, EventArgs e)
        {
            monthlyreport.ExportSettings.HideStructureColumns = true;
            monthlyreport.ExportSettings.SuppressColumnDataFormatStrings = false;
            monthlyreport.ExportSettings.ExportOnlyData = false;
            monthlyreport.ExportSettings.IgnorePaging = true;
            monthlyreport.ExportSettings.Excel.DefaultCellAlignment = HorizontalAlign.Center;
            monthlyreport.MasterTableView.AllowPaging = false;
            monthlyreport.MasterTableView.Rebind();
            monthlyreport.ExportSettings.OpenInNewWindow = true;


            //Excel Format
            monthlyreport.ExportSettings.Excel.Format = GridExcelExportFormat.Xlsx;


            monthlyreport.MasterTableView.ExportToExcel();
        }

        protected void sal_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            monthyearr.Text = sal.SelectedValue.ToString() + mah.SelectedValue.ToString();
            RadGrid1.DataBind();
            
        }

        protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
        {

            if (RadioButton1.Checked)
            {
                monthyearselected.Visible = false;
                inmah.Visible = true;
            }
           
        }

        protected void byMonthYear_CheckedChanged(object sender, EventArgs e)
        {
            if(byMonthYear.Checked)
            {
                monthyearselected.Visible = true;
                inmah.Visible = false;
            }
           
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            RadGrid1.ExportSettings.HideStructureColumns = true;
            RadGrid1.ExportSettings.SuppressColumnDataFormatStrings = false;
            RadGrid1.ExportSettings.ExportOnlyData = false;
            RadGrid1.ExportSettings.IgnorePaging = true;
            RadGrid1.ExportSettings.Excel.DefaultCellAlignment = HorizontalAlign.Center;
            RadGrid1.MasterTableView.AllowPaging = false;
            RadGrid1.MasterTableView.Rebind();
            RadGrid1.ExportSettings.OpenInNewWindow = true;


            //Excel Format
            RadGrid1.ExportSettings.Excel.Format = GridExcelExportFormat.Xlsx;


            RadGrid1.MasterTableView.ExportToExcel();
        }

        protected void mah_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            monthyearr.Text = sal.SelectedValue.ToString() + mah.SelectedValue.ToString();
            RadGrid1.DataBind();
        }
    }
}