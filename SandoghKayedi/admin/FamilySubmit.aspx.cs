﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace SandoghKayedi.admin
{
    public partial class FamilySubmit : System.Web.UI.Page
    {
        public string[] res;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["type"]==null || Session["type"].ToString() != "Admin")
            {
                Response.Redirect("/Login.aspx");
            }
           
           PersianDateTime now = PersianDateTime.Now;
           string persianDate = now.ToString(PersianDateTimeFormat.Date);
           string s = persianDate;
           datepicker.Attributes["onclick"] = "PersianDatePicker.Show(this, '" + s + "');";
        }
        protected string GetLastMonth(string UserID)
        {
            
            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;


            cmd.CommandText = "GetLastMonthYear";
            cmd.Parameters.Add("@ParentID", SqlDbType.Int).Value = UserID;
            cmd.Parameters.Add("@PlusOne", SqlDbType.Int).Value = 1;



           
            string month;
            conn.Open();
            SqlDataReader dr;
            dr = cmd.ExecuteReader();

            
                dr.Read();
                month = dr["Lastmonth"].ToString();
                conn.Close();
 return month;


            
            
         
           
               
           

        }
        protected string GetLastYear(string UserID)
        {

            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;


            cmd.CommandText = "GetLastMonthYear";
            cmd.Parameters.Add("@ParentID", SqlDbType.Int).Value = UserID;
            cmd.Parameters.Add("@PlusOne", SqlDbType.Int).Value = 1;



            string year;
            conn.Open();
            SqlDataReader dr;
            dr = cmd.ExecuteReader();


            dr.Read();
            year = dr["Lastyear"].ToString();
            conn.Close();
            return year;









        }

        private void pupulateTextbox(){
             var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            string monthyear = GetLastYear(Rad.SelectedValue.ToString()) + GetLastMonth(Rad.SelectedValue.ToString());

            cmd.CommandText = "GetPardakhtiMahaneFamily";
            cmd.Parameters.Add("@ParentID", SqlDbType.Int).Value = Rad.SelectedValue.ToString();
            cmd.Parameters.Add("@date", SqlDbType.Int).Value = monthyear;


            //SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
           // returnParameter.Direction = ParameterDirection.ReturnValue;

            conn.Open();
            SqlDataReader dr;
            dr=cmd.ExecuteReader();

             if (dr.HasRows)
                {
                    dr.Read();
                    Cost.Text=dr["SeparatePardakhti"].ToString();
                    ComboMonth.SelectedValue = GetLastMonth(Rad.SelectedValue.ToString());
                    TextLastyear.Text = GetLastYear(Rad.SelectedValue.ToString());
                }
//int rtn = (int)returnParameter.Value;
            //TextBox1.Text = rtn.ToString();
             conn.Close();
        }
        protected void Rad_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {

            pupulateTextbox();
            

        }

        protected void SubmitFamily_Click(object sender, EventArgs e)
        {
            if (Paytype.SelectedItem.Value != "درگاه")
            {
                var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = con.ConnectionString.ToString();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                string monthyear = GetLastYear(Rad.SelectedValue.ToString()) + GetLastMonth(Rad.SelectedValue.ToString());

                cmd.CommandText = "SubmitPardakhtiFamily";
                cmd.Parameters.Add("@ParentID", SqlDbType.Int).Value = Rad.SelectedValue.ToString();
                cmd.Parameters.Add("@date", SqlDbType.NVarChar).Value = monthyear;
                cmd.Parameters.Add("@PayType", SqlDbType.NVarChar).Value = Paytype.SelectedValue.ToString();
                cmd.Parameters.Add("@Fishdate", SqlDbType.NVarChar).Value = datepicker.Text;
                cmd.Parameters.Add("@fish", SqlDbType.NVarChar).Value = fish.Text;
                cmd.Parameters.Add("@tozihat", SqlDbType.NVarChar).Value = desc.Text;


                //SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
                // returnParameter.Direction = ParameterDirection.ReturnValue;

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
                GreenMessagebox.Visible = true;
            }
            else
            {
                string orderid="0";
                var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = con.ConnectionString.ToString();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = System.Data.CommandType.Text;

                string monthyear = GetLastYear(Rad.SelectedValue.ToString()) + GetLastMonth(Rad.SelectedValue.ToString());

                cmd.CommandText = "INSERT INTO tbl_Gateway (LocalDate,LocalTime,FamilyID,MonthYear,Cost) OUTPUT Inserted.OrderID VALUES('" + DateTime.Now.ToShortDateString() + "','" + DateTime.Now.ToShortDateString() + "','" + Rad.SelectedValue.ToString() + "','" + monthyear + "','" + Cost.Text.Replace(",", "") + "');";
                SqlDataReader dr;
                conn.Open();

                dr= cmd.ExecuteReader();
                if (dr.Read())
                    orderid = dr[0].ToString();
                conn.Close();


                using (var webService = new ir.shaparak.bpm.PaymentGatewayImplService())
                {
                    var request = webService.bpPayRequest(publicdata.mellatTerminal, publicdata.mellatUser, publicdata.mellatPassword, long.Parse(orderid), long.Parse(Cost.Text.Replace(",", "")), DateTime.Now.ToString("yyyymmdd"), DateTime.Now.ToString("HHmmss"),(Rad.SelectedItem.Text + " "+ ComboMonth.SelectedItem.Text + " " +TextLastyear.Text), Request.Url.GetLeftPart(UriPartial.Authority) + "/admin/MellatVerify.aspx", 0);

                    res = request.Split(',');
                    if (res[0] == "0")
                    {

                        string Url = publicdata.mellatGateway;
                        string formId = "myForm1";

                        StringBuilder htmlForm = new StringBuilder();
                        htmlForm.AppendLine("<html>");
                        htmlForm.AppendLine(String.Format("<body onload='document.forms[\"{0}\"].submit()'>", formId));
                        htmlForm.AppendLine(String.Format("<form id='{0}' method='POST' action='{1}'>", formId, Url));
                        htmlForm.AppendLine("<input type='hidden' id='RefId' name='RefId' value='" + res[1].ToString() + "' />");
                        htmlForm.AppendLine("</form>");
                        htmlForm.AppendLine("</body>");
                        htmlForm.AppendLine("</html>");

                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.Write(htmlForm.ToString());
                        HttpContext.Current.Response.End();
                    }
                    else{
                        GatewayClass gc = new GatewayClass();
                        dargahmessage.Text = gc.DesribtionStatusCode(int.Parse(res[0]));
                        dargahmessage.ForeColor = Color.Red;

                    }
                }
            }
        }

        protected void SubmitNewFish_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }

        protected void Radgrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "AcnonAc")
            {
                GridDataItem item = e.Item as GridDataItem;

                string PersonalID = item["PersonalID"].Text;

                 var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = con.ConnectionString.ToString();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "SwapActive";
                cmd.Parameters.Add("@UserID", SqlDbType.NVarChar).Value = PersonalID;

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
                pupulateTextbox();
                Radgrid.DataBind();
            

               
            }
        }

        protected void Paytype_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (Paytype.SelectedItem.Value == "درگاه")
            {
               dargahbox.Visible=true;
               datebox.Visible = false;
               fishbox.Visible = false;
               SubmitFamilydargah.Visible = true;
               SubmitFamily.Visible = false;
               tozihatbox.Visible = false;
            }
            else
            {
                dargahbox.Visible = false;
                datebox.Visible = true;
                fishbox.Visible = true;
                SubmitFamilydargah.Visible = false;
                SubmitFamily.Visible = true;
                tozihatbox.Visible = true;

            }



        }

        protected void Radgrid_DataBound(object sender, EventArgs e)
        {
            if (Radgrid.Items.Count > 0)
            {
                SubmitFamily.Enabled = true;
                SubmitFamilydargah.Enabled = true;
            }
        }

  

     
  


       

  
    }
}