﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ad.Master" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="SandoghKayedi.admin.Profile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        html *{
            font-family:'B Yekan';   

              

        }
        .gridFont{
                font-size:15px;
                font-weight:normal

        }
    </style>
    
        <!--header start-->
                                                <asp:ScriptManager runat="server"></asp:ScriptManager>
    <%-- <div style="position:fixed; left:50%; top:50%; background-color:gray; z-index:100600;"> 
       <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="up" runat="server">
            <ProgressTemplate>
                <img alt="..." src="/images/loading.gif" />
            </ProgressTemplate>
        </asp:UpdateProgress>
         </div>--%>

 <asp:UpdatePanel ID="up" runat="server">
     
     <ContentTemplate>
         

    
                <!-- page start-->
                <div class="row">
                    <aside class="profile-nav col-lg-3">
                        <section class="panel">
                            <div class="user-heading round">
                                <a href="#">
                                   <asp:Image runat="server" ID="profilepic" />
                                </a>
                                <h1><asp:Label runat="server" ID="FLN"></asp:Label></h1>
                            </div>

                            <ul class="nav nav-pills nav-stacked">
                        <li><a ><i class="icon-home"></i><asp:Button runat="server" ID="familyShow"  Text="مشاهده اعضا" BackColor="Transparent" BorderStyle="None" OnClick="familyShow_Click"/> </a></li>

                                <li><a ><i class="icon-desktop"></i><asp:Button runat="server" ID="vamShow"  Text="مشاهده وام های دریافتی" BackColor="Transparent" BorderStyle="None" OnClick="vamShow_Click"/> </a></li>
                                <li><a ><i class="icon-dollar"></i><asp:Button runat="server" ID="Pardakhtishow"  Text="مشاهده پرداختی ها" BackColor="Transparent" BorderStyle="None" OnClick="Pardakhtishow_Click"/> </a></li>

                                  </ul>

                        </section>
                    </aside>
                    <aside class="profile-info col-lg-9">
                        
                        <section class="panel">
                            
                            <div class="panel-body bio-graph-info">
                                <h1>مشخصات کلی</h1>
                                <div class="row">
                                      <div class="bio-row" style="color:red">
                                        <p><span style="width:145px">شماره عضویت خانوار:</span><asp:label runat="server" ID="SSIDFamily"></asp:label></p>
                                    </div>
                                    <div class="bio-row">
                                        <p><span>نام: </span><asp:label runat="server" ID= "name"></asp:label></p>
                                    </div>
                                    <div class="bio-row">
                                        <p><span>نام خانوادگی: </span><asp:label runat="server" ID="family"></asp:label></p>
                                    </div>
                                    <div class="bio-row">
                                        <p><span>نام پدر: </span><asp:label runat="server" ID="father"></asp:label></p>
                                    </div>
                                    <div class="bio-row">
                                        <p><span>تاریخ تولد</span><asp:label runat="server" ID="DOB"></asp:label></p>
                                    </div>
                                      <div class="bio-row">
                                        <p><span>کد ملی</span><asp:label runat="server" ID="mellicode"></asp:label></p>
                                    </div>
                                    

                                     </div>
                                    
                                   
                                </div>
                            </div>
                        </section>
                        <section>
                            <div class="row">
                                <div class="col-lg-12" runat="server" id="FamilyRow" visible="false">
                                    <div class="panel">
                                        <div class="panel-body">
                                           <h4>اعضا</h4>
                                         <telerik:RadGrid runat="server" ItemStyle-CssClass="gridFont" AlternatingItemStyle-CssClass="gridFont"   AlternatingItemStyle-Font-Names="B yekan" HeaderStyle-Font-Names="b yekan" ItemStyle-Font-Names="B yekan" CellSpacing="-1" DataSourceID="SqlDataSource2" GridLines="Both" GroupPanelPosition="Top" ID="Radgrid"  >
                                        <MasterTableView DataSourceID="SqlDataSource2" AutoGenerateColumns="False">
                                            <Columns>
<%--                                                <telerik:GridImageColumn DataImageUrlFields="PersonalID" DataImageUrlFormatString="/admin/ProfilePicture/{0}.jpg" AlternateText=" " ImageHeight="32" ImageWidth="32"></telerik:GridImageColumn>--%>
                                                <telerik:GridBoundColumn DataField="LastSSID" HeaderText="شماره عضویت" SortExpression="PersonalID" UniqueName="PersonalID" DataType="System.Int64" FilterControlAltText="Filter PersonalID column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="FLN" HeaderText="نام و نام خانوادگی" SortExpression="FLN" UniqueName="FLN" FilterControlAltText="Filter FLN column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="Loanturn" HeaderText="نوبت وام" SortExpression="Loanturn" UniqueName="Loanturn" DataType="System.Int64" FilterControlAltText="Filter Loanturn column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="Fathername" HeaderText="نام پدر" SortExpression="Fathername" UniqueName="Fathername" DataType="System.Int64" FilterControlAltText="Filter SumOzviat column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="DOB" HeaderText="تاریخ تولد" SortExpression="DOB" UniqueName="DOB" DataType="System.Int64" FilterControlAltText="Filter SumVam column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="MelliCode" HeaderText="کد ملی" SortExpression="MelliCode" UniqueName="MelliCode" DataType="System.Int64" FilterControlAltText="Filter Active column"></telerik:GridBoundColumn>
                                              
                                            </Columns>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                    <asp:SqlDataSource runat="server" ID="SqlDataSource2" ConnectionString='<%$ ConnectionStrings:cnnstring %>' ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM [InformationView] WHERE ([ParentID] = @personalId) "  >
                                        

                                        <SelectParameters>
                                            <asp:SessionParameter Name="personalId" SessionField="personalId"  Type="String" ></asp:SessionParameter>
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12" runat="server" id="vam" visible="false">
                                    <div class="panel">
                                        <div class="panel-body">
                                           <h4>وام های دریافتی</h4>
                                            <telerik:RadGrid ItemStyle-CssClass="gridFont" AlternatingItemStyle-CssClass="gridFont" runat="server" CellSpacing="-1" DataSourceID="SqlDataSourceVam" GridLines="Both" GroupPanelPosition="Top" AllowPaging="True">
                                                <MasterTableView Dir="RTL" DataSourceID="SqlDataSourceVam" AutoGenerateColumns="False">
                                                    <Columns>
                                                      <telerik:GridBoundColumn DataField="LastSSID" HeaderText="شماره عضویت" SortExpression="LastSSID" UniqueName="LastSSID" FilterControlAltText="Filter Firstname column"></telerik:GridBoundColumn>

                                                        <telerik:GridBoundColumn DataField="FLN" HeaderText="نام و نام خانوادگی" SortExpression="Firstname" UniqueName="Firstname" FilterControlAltText="Filter Firstname column"></telerik:GridBoundColumn>
                                          <telerik:GridBoundColumn DataField="marhale"  HeaderText="دوره وام" SortExpression="marhale" UniqueName="marhale" FilterControlAltText="Filter vam column"></telerik:GridBoundColumn>

                                                         <telerik:GridBoundColumn DataField="vam" HeaderText="مبلغ وام" SortExpression="vam" UniqueName="vam" FilterControlAltText="Filter vam column"></telerik:GridBoundColumn>

                                                        <telerik:GridBoundColumn DataField="monthh" HeaderText="ماه" SortExpression="monthyear" UniqueName="monthyear" FilterControlAltText="Filter monthyear column"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Yearr" HeaderText="سال" SortExpression="monthyear" UniqueName="monthyear" FilterControlAltText="Filter monthyear column"></telerik:GridBoundColumn>
                                                        
                                                        <telerik:GridBoundColumn DataField="fish" HeaderText="شماره فیش" SortExpression="fish" UniqueName="fish" FilterControlAltText="Filter monthyear column"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Fishdate" HeaderText="تاریخ فیش" SortExpression="Fishdate" UniqueName="Fishdate" FilterControlAltText="Filter monthyear column"></telerik:GridBoundColumn>

                                                           </Columns>
                                                </MasterTableView>
                                            </telerik:RadGrid>
                                            <asp:SqlDataSource runat="server" ID="SqlDataSourceVam" ConnectionString='<%$ ConnectionStrings:cnnstring %>' ProviderName="System.Data.SqlClient" SelectCommand="SELECT replace(convert(varchar,convert(Money, vam),1),'.00','') as vam, [Firstname]+ ' '+ [Lastname] as FLN, left(monthyear,4) as Yearr,right(monthyear,2) as monthh,LastSSID,Fishdate,fish,marhale FROM [VamWithNames] WHERE ([ssidf] = @ssidf) order by monthyear">
                                                <SelectParameters>
                                                    <asp:SessionParameter SessionField="personalID" Name="ssidf" Type="String"></asp:SessionParameter>
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12" runat="server" id="pardakhti" visible="false">
                                    <div class="panel">
                                        <div class="panel-body">
                                           <h4>پرداختی ها</h4>
                                            <telerik:RadGrid ItemStyle-CssClass="gridFont" AlternatingItemStyle-CssClass="gridFont" AllowPaging="true" runat="server" CellSpacing="-1" DataSourceID="SqlDataSourcePardakhti" GridLines="Both" GroupPanelPosition="Top">
                                                <MasterTableView DataSourceID="SqlDataSourcePardakhti" AutoGenerateColumns="False">
                                                    <Columns>
                                                 <telerik:GridBoundColumn DataField="LastSSID" HeaderText="شماره عضویت" SortExpression="LastSSID" UniqueName="LastSSID" FilterControlAltText="Filter Firstname column"></telerik:GridBoundColumn>

                                                <telerik:GridBoundColumn DataField="FLN" HeaderText="نام و نام خانوادگی" SortExpression="Firstname" UniqueName="Firstname" FilterControlAltText="Filter Firstname column"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="plus" HeaderText="مبلغ پرداختی" SortExpression="plus" UniqueName="plus" FilterControlAltText="Filter plus column"></telerik:GridBoundColumn>

                                                        <telerik:GridBoundColumn DataField="monthh" HeaderText="ماه" SortExpression="monthyear" UniqueName="monthyear" FilterControlAltText="Filter monthyear column"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Yearr" HeaderText="سال" SortExpression="monthyear" UniqueName="monthyear" FilterControlAltText="Filter monthyear column"></telerik:GridBoundColumn>

                                                          <telerik:GridBoundColumn DataField="fish" HeaderText="شماره فیش" SortExpression="fish" UniqueName="fish" FilterControlAltText="Filter monthyear column"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Fishdate" HeaderText="تاریخ فیش" SortExpression="Fishdate" UniqueName="Fishdate" FilterControlAltText="Filter monthyear column"></telerik:GridBoundColumn>
                                                    </Columns>
                                                </MasterTableView>
                                            </telerik:RadGrid>

                                            <asp:SqlDataSource runat="server" ID="SqlDataSourcePardakhti" ConnectionString='<%$ ConnectionStrings:cnnstring %>' ProviderName="System.Data.SqlClient" SelectCommand="SELECT left(monthyear,4) as Yearr,right(monthyear,2) as monthh, [Firstname]+' '+ [Lastname] as FLN, replace(convert(varchar,convert(Money, plus),1),'.00','') as plus,LastSSID,fish,fishDate FROM [PardakhtiWithNames] WHERE ([ssidf] = @ssidf) order by monthyear">
                                                <SelectParameters>
                                                    <asp:SessionParameter SessionField="personalID" Name="ssidf" Type="String"></asp:SessionParameter>
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </section>
                    </aside>
                </div>

                <!-- page end-->
        
     
  </ContentTemplate>
 </asp:UpdatePanel>
   
</asp:Content>
