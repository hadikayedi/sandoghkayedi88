﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ad.Master" AutoEventWireup="true" CodeBehind="Dates.aspx.cs" Inherits="SandoghKayedi.admin.Dates" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <div class="col-lg-9" id="divDates" runat="server">
        <section class="panel">
            <header class="panel-heading">
                دوره های صندوق                          
            </header>

            <asp:SqlDataSource runat="server" ID="SqlDataSource" ConnectionString='<%$ ConnectionStrings:cnnstring %>' ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM [payenew] order by id asc"></asp:SqlDataSource>

            <telerik:RadGrid OnItemCommand="RadGrid1_ItemCommand" ItemStyle-Font-Names="b yekan" ItemStyle-Font-Size="14px" AlternatingItemStyle-Font-Names="b yekan" AlternatingItemStyle-Font-Size="14px" HeaderStyle-Font-Names="b yekan" HeaderStyle-Font-Size="16px" ClientSettings-EnablePostBackOnRowClick="true" runat="server" ID="RadGrid1" CellSpacing="-1" GridLines="Both" GroupPanelPosition="Top" AutoGenerateColumns="False" DataSourceID="SqlDataSource">
                <ClientSettings>
                    <Selecting AllowRowSelect="True"></Selecting>
                </ClientSettings>

                <MasterTableView AutoGenerateColumns="false" DataSourceID="SqlDataSource">
                    <Columns>
                        <telerik:GridBoundColumn DataField="Id" ReadOnly="True" HeaderText="کد دوره"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="marhale" HeaderText="مرحله"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="fr" HeaderText="شروع"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="t" HeaderText="پایان"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="antrack" HeaderText="آنتراک"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="haghozviat" HeaderText="حق عضویت"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="idvam" HeaderText="کد وام"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="vam" HeaderText="مبلغ وام"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="tedad" HeaderText="تعداد بازپرداخت"></telerik:GridBoundColumn>


                        <telerik:GridButtonColumn CommandName="Select" HeaderText="ویرایش" Text="ویرایش" UniqueName="Select">
                        </telerik:GridButtonColumn>

                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </section>
    </div>

    <div class="col-lg-3" id="divEdit" visible="false" runat="server">
        <section class="panel">
        <header class="panel-heading">
            ویرایش                         
        </header>
        

            <div class="input-group m-bot15" style="width: 250px">
                <span class="input-group-addon">کد دوره</span>
                <asp:TextBox BackColor="White" ID="IDtemp" Enabled="false" ReadOnly="true" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
            </div>
            <div class="input-group m-bot15" style="width: 250px">
                <span class="input-group-addon">مرحله</span>
                <asp:TextBox BackColor="White" ID="marhaletemp" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
            </div>
            <div class="input-group m-bot15" style="width: 250px">
                <span class="input-group-addon">شروع</span>
                <asp:TextBox BackColor="White" ID="frtemp" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
            </div>
            <div class="input-group m-bot15" style="width: 250px">
                <span class="input-group-addon">پایان</span>
                <asp:TextBox BackColor="White" ID="ttemp" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
            </div>
            <div class="input-group m-bot15" style="width: 250px">
                <span class="input-group-addon">آنتراک</span>
                <asp:TextBox BackColor="White" ID="antacktemp" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
            </div>
            <div class="input-group m-bot15" style="width: 250px">
                <span class="input-group-addon">حق عضویت</span>
                <asp:TextBox BackColor="White" ID="haghozviattemp" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
            </div>
            <div class="input-group m-bot15" style="width: 250px">
                <span class="input-group-addon">کد وام</span>
                <asp:TextBox BackColor="White" ID="idvamtemp" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
            </div>
            <div class="input-group m-bot15" style="width: 250px">
                <span class="input-group-addon">مبلغ وام</span>
                <asp:TextBox BackColor="White" ID="vamtemp" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
            </div>
            <div class="input-group m-bot15" style="width: 250px">
                <span class="input-group-addon">تعداد بازپرداخت</span>
                <asp:TextBox BackColor="White" ID="tedadtemp" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
            </div>

            <asp:Button ID="editDates" runat="server" OnClick="editDates_Click" Enabled="true" CssClass="btn btn-info" Text="ثبت" />

        </section>

    </div>
</asp:Content>
