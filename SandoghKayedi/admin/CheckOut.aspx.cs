﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SandoghKayedi.admin
{
    public partial class CheckOut : System.Web.UI.Page
    {
        PersianDateTime now;
        string persianDate;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["type"]==null || Session["type"].ToString() != "Admin")
            {
                Response.Redirect("/Login.aspx");
            }
            now = PersianDateTime.Now;
             persianDate = now.ToString(PersianDateTimeFormat.Date);
            string s = persianDate;
            date.Attributes["onclick"] = "PersianDatePicker.Show(this, '" + s + "');";
        }

        protected string popLastvam(bool comma)
        {
            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            cmd.CommandText = "GetVam";

            SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
            returnParameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@UserID", SqlDbType.NVarChar).Value = Rad.SelectedValue.ToString();

            conn.Open();
            cmd.ExecuteNonQuery();

            if(!comma)
            return returnParameter.Value.ToString();
            else
                return String.Format("{0:N0}", Convert.ToDouble(returnParameter.Value.ToString()));

        }
        protected string popSumbazpardakht(bool comma)
        {
            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            cmd.CommandText = "GetSumPardakhti";

            SqlParameter returnParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int);
            returnParameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@ssid", SqlDbType.NVarChar).Value = Rad.SelectedValue.ToString();

            conn.Open();
            cmd.ExecuteNonQuery();
            if (!comma)
            return returnParameter.Value.ToString();
            else
                return String.Format("{0:N0}", Convert.ToDouble(returnParameter.Value.ToString()));

        }
        protected string popMandevam(bool comma)
        {
            string mandevam=(int.Parse(String.Format("{0}", Convert.ToDouble(LastVam.Text))) - int.Parse(String.Format("{0}", Convert.ToDouble(Sumbazpardakht.Text)))).ToString();
            if (!comma)
                return mandevam;
            else
                return String.Format("{0:N0}", Convert.ToDouble(mandevam));
        }
        protected string popSumozviat(bool comma)
        {
            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            cmd.CommandText = "GetSumOzviat";

            cmd.Parameters.Add("@ssid", SqlDbType.NVarChar).Value = Rad.SelectedValue.ToString();

            conn.Open();
            SqlDataReader dr;
            
            dr=cmd.ExecuteReader();
            dr.Read();
            string sum = dr["SumOzviat"].ToString();
            
            conn.Close();
            if(!comma)
           return sum;
            else
           return String.Format("{0:N0}", Convert.ToDouble(sum));
            
        }
        protected string popMande(bool comma)
        {
            string popmande=(int.Parse(String.Format("{0}", Convert.ToDouble(Sumozviat.Text))) - int.Parse(String.Format("{0}", Convert.ToDouble(MandeVam.Text)))).ToString();
            if (!comma)
                return popmande;
            else

                return String.Format("{0:N0}", Convert.ToDouble(popmande));
        }
        protected void popLastdate()
        {
            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            cmd.CommandText = "GetLastMonthYearNafar";

            
            cmd.Parameters.Add("@UserID", SqlDbType.NVarChar).Value = Rad.SelectedValue.ToString();

            conn.Open();
            SqlDataReader dr;

            dr = cmd.ExecuteReader();
            dr.Read();
            string date = dr["Lastmonthfarsi"].ToString() + ' ' + dr["Lastyear"].ToString();
            lastdate.Text = date;
            conn.Close();
          

        }
        protected void popStatus()
        {
            if ((int.Parse(String.Format("{0}", Convert.ToDouble(Sumozviat.Text))) - int.Parse(String.Format("{0}", Convert.ToDouble(MandeVam.Text)))) > 0)
            {
                Status.Text = "بستانکار";
                Status.ForeColor = Color.Green;
            }
            else if ((int.Parse(String.Format("{0}", Convert.ToDouble(Sumozviat.Text))) - int.Parse(String.Format("{0}", Convert.ToDouble(MandeVam.Text)))) < 0)

            {
                Status.Text = "بدهکار";
                Status.ForeColor = Color.Red;
            }
            else
            {
                Status.Text = "برابر";
                Status.ForeColor = Color.Black;
            }
        }
        protected void Rad_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            vampanel.Visible = true;
            pardakhtipanel.Visible = true;
            Abstractpanel.Visible = true;
            ozviatpanel.Visible = true;
            LastVam.Text = popLastvam(true);
            Sumbazpardakht.Text= popSumbazpardakht(true);
           MandeVam.Text= popMandevam(true);
          Sumozviat.Text=  popSumozviat(true);
           Mande.Text= popMande(true);
           popLastdate();
            popStatus();
            if(Mande.Text=="0")
            {
                fish.Text = "0";
                fish.Enabled = false;
                date.Text = "0";
                date.Enabled = false;
            }

        

        }

        protected void deactiveUser(string userID)
        {
            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "update tbl_Information set Active=0 where PersonalID='"+userID+"'";
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        protected Boolean fishExist(string fish,string date)
        {
            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "select * from tbl_CheckOut where Fish='" + fish + "' and Fishdate='" + date + "'";
            SqlDataReader dr;
            Boolean exist;
            conn.Open();
            dr=cmd.ExecuteReader();
            dr.Read();
            
            if (dr.HasRows)
                exist= true;
            else
                exist= false;
            conn.Close();
            return exist;

        }
        protected void CheckOutButton_Click(object sender, EventArgs e)
        {
            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "insert into tbl_CheckOut values ('" + Rad.SelectedValue.ToString() + "','" + popLastvam(false) + "','" + popSumbazpardakht(false) + "','" + popMandevam(false) + "','" + popSumozviat(false) + "','" + popMande(false) + "','" + Status.Text + "','" + fish.Text + "','" + date.Text + "','" + persianDate + "','"+Tozihat.Text+"')";

            conn.Open();
            if (!fishExist(fish.Text, date.Text))
            {
                if (cmd.ExecuteNonQuery() > 0)
                {
                    deactiveUser(Rad.SelectedValue.ToString());
                    messagebox.Text = "عضو مورد نظر با موفقیت تسویه حساب شد";
                    messagebox.ForeColor = Color.Black;

                    messagebox.BackColor = Color.LightGreen;
                    messagebox.Visible = true;
                }
                else
                {
                    messagebox.Text = "لطفا با مدیریت تماس حاصل فرمایید";
                    messagebox.BackColor = Color.Red;
                    messagebox.ForeColor = Color.White;
                    messagebox.Visible = true;
                }
            }
            else
            {
                messagebox.Text = "شماره فیش وارده تکراری می باشد";
                messagebox.BackColor = Color.Red;
                messagebox.ForeColor = Color.White;

                messagebox.Visible = true;
            }
                
            conn.Close();

        }
    }
}