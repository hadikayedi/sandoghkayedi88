﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ad.Master" AutoEventWireup="true" CodeBehind="MonthlyReport.aspx.cs" Inherits="SandoghKayedi.admin.MonthlyReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <style>
        .Treeviewheader{
            font-family:"B Yekan";
            font-size:14px;
            font-weight:bold;

        }
        .Treeviewitem{
            font-family:"B Yekan";
            font-size:13px;

        }
        .Treeviewalter{
            font-family:"B Yekan";
            font-size:13px;


        }
        div.rcbSlide .RadComboBoxDropDown .rcbScroll
       {
 		font-family: 'B Yekan';
         font-size:14px;
    }
    </style>
    <asp:ScriptManager runat="server"></asp:ScriptManager>
     
     <div class="col-lg-4" id="div2" visible="true"  runat="server"  >
                        <section class="panel">
                            <header class="panel-heading">
جست و جو
                            </header>
                            

                                 
                            
                          
                             <div class="input-group m-bot15" style="width:350px" >
                                <span class="input-group-addon  blacktextbox" style="color:black" ><asp:RadioButton ID="byMonthYear" AutoPostBack="true"   GroupName="t"  runat="server" OnCheckedChanged="byMonthYear_CheckedChanged" />
                                    بر اساس ماه</span>
                                 <telerik:RadComboBox runat="server" CssClass="form-control blacktextbox" Font-Size="14px" Font-Names="B yekan" EmptyMessage="انتخاب نمایید" AllowCustomText="true" Filter="Contains" AutoPostBack="true" ID="mah" OnSelectedIndexChanged="mah_SelectedIndexChanged"  >
                                     <Items>
                                         <telerik:RadComboBoxItem runat="server" Text="فروردین" Value="01"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="اردیبهشت" Value="02"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="خرداد" Value="03"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="تیر" Value="04"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="مرداد" Value="05"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="شهریور" Value="06"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="مهر" Value="07"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="آبان" Value="08"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="آذر" Value="09"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="دی" Value="10"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="بهمن" Value="11"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="اسفند" Value="12"></telerik:RadComboBoxItem>
                                     </Items>
                                 </telerik:RadComboBox>
                                 <telerik:RadComboBox runat="server" CssClass="form-control blacktextbox" Font-Size="14px" Font-Names="B yekan" EmptyMessage="انتخاب نمایید" AllowCustomText="true" Filter="Contains" AutoPostBack="true" ID="sal" OnSelectedIndexChanged="sal_SelectedIndexChanged" >
                                 <Items>
                                         <telerik:RadComboBoxItem runat="server" Text="1388" Value="1388"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="1389" Value="1389"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="1390" Value="1390"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="1391" Value="1391"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="1392" Value="1392"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="1393" Value="1393"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="1394" Value="1394"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="1395" Value="1395"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="1396" Value="1396"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="1397" Value="1397"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="1398" Value="1398"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="1399" Value="1399"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="1400" Value="1400"></telerik:RadComboBoxItem>

                                     </Items>
                                      </telerik:RadComboBox>
                                 <asp:TextBox ID="monthyearr" Visible="false" Text="138810" runat="server"></asp:TextBox>
                             </div>
                                       <div class="input-group m-bot15" style="width:350px" >
                                <span class="input-group-addon form-control blacktextbox" style="color:black"><asp:RadioButton  ID="RadioButton1" Checked="true" AutoPostBack="true" GroupName="t"  runat="server"  OnCheckedChanged="RadioButton1_CheckedChanged" />
                                    بر اساس این ماه</span>

                                           </div>
                            
 
                           
<br />
                                     
                            </section>
            </div>

    <div class="col-lg-8" id="inmah" visible="true"  runat="server"  >
                        <section class="panel">
                            <header class="panel-heading">


گزارش ماهانه این ماه                          </header>

    <telerik:RadGrid  runat="server" ID="monthlyreport" HeaderStyle-CssClass="Treeviewheader" ItemStyle-CssClass="Treeviewitem" AlternatingItemStyle-CssClass="Treeviewalter" CellSpacing="-1" GridLines="Both" GroupPanelPosition="Top" DataSourceID="SqlDataSource1">
        <ClientSettings>
            <Selecting AllowRowSelect="True"></Selecting>
        </ClientSettings>

        <MasterTableView HeaderStyle-CssClass="Treeviewheader" Dir="RTL" ItemStyle-CssClass="Treeviewitem" AlternatingItemStyle-CssClass="Treeviewalter" AutoGenerateColumns="false" DataSourceID="SqlDataSource1">
            
            <Columns>
                <telerik:GridBoundColumn DataField="FLN" HeaderText="نام و نام خانوادگی"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="childcount" HeaderText="تعداد اعضا"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="LastMonthFarsi" HeaderText="ماه"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="LastYear" HeaderText="سال"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="mablagh" HeaderText="مبلغ (ریال)"></telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="شماره فیش"></telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn HeaderText="تاریخ فیش"></telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn HeaderText="توضیحات"></telerik:GridTemplateColumn>


            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
    <br />
                <asp:Button Text="دریافت فایل اکسل" CssClass="btn btn-info"   runat="server" ID="btnExportexcel"  OnClick="btnExportexcel_Click" />

    <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:cnnstring %>' ProviderName="System.Data.SqlClient" SelectCommand="GetMonthlyReport" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
</section>
        </div>
                                     <div class="col-lg-8" id="monthyearselected" visible="false" runat="server">
                        <section class="panel">
                            <header class="panel-heading">


گزارش ماهانه  ماه به خصوص                          </header>

    <telerik:RadGrid runat="server" ID="RadGrid1" HeaderStyle-CssClass="Treeviewheader" ItemStyle-CssClass="Treeviewitem" AlternatingItemStyle-CssClass="Treeviewalter" CellSpacing="-1" GridLines="Both" GroupPanelPosition="Top" DataSourceID="SqlDataSource1">
        <ClientSettings>
            <Selecting AllowRowSelect="True" ></Selecting>
            
        </ClientSettings>

        <MasterTableView HeaderStyle-CssClass="Treeviewheader" Dir="RTL" ItemStyle-CssClass="Treeviewitem" AlternatingItemStyle-CssClass="Treeviewalter" AutoGenerateColumns="false" DataSourceID="SqlDataSource2">
            
            <Columns>
                <telerik:GridBoundColumn DataField="FLN" HeaderText="نام و نام خانوادگی"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="childcount" HeaderText="تعداد اعضا"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="LastMonthFarsi" HeaderText="ماه"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="LastYear" HeaderText="سال"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="mablagh" HeaderText="مبلغ (ریال)"></telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="شماره فیش"></telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn HeaderText="تاریخ فیش"></telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn HeaderText="توضیحات"></telerik:GridTemplateColumn>


            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
    <br />
                             
                <asp:Button Text="دریافت فایل اکسل" CssClass="btn btn-info"   runat="server" ID="Button1"  OnClick="Button1_Click" />

                            <asp:SqlDataSource runat="server" ID="SqlDataSource2" ConnectionString='<%$ ConnectionStrings:cnnstring %>' ProviderName="System.Data.SqlClient" SelectCommand="_GetMonthlyReportWithCount" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="monthyearr" PropertyName="Text" DefaultValue="138810" Name="Date" Type="String"></asp:ControlParameter>

                                </SelectParameters>
                            </asp:SqlDataSource>
                            
</section>
        </div>
   
</asp:Content>
