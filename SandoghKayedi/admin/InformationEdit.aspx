﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ad.Master" AutoEventWireup="true" CodeBehind="InformationEdit.aspx.cs" Inherits="SandoghKayedi.admin.InformationEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <div class="col-lg-3" id="divselect" visible="true" runat="server">
                <section class="panel">
                    <header class="panel-heading">
                        خانواده ها                           
                    </header>

                    لطفا روی یک خانواده کلیک نمایید:
    <telerik:RadGrid ItemStyle-Font-Names="b yekan" ItemStyle-Font-Size="14px" AlternatingItemStyle-Font-Names="b yekan" AlternatingItemStyle-Font-Size="14px" HeaderStyle-Font-Names="b yekan" HeaderStyle-Font-Size="16px" ClientSettings-EnablePostBackOnRowClick="true" runat="server" ID="rad" CellSpacing="-1" OnSelectedIndexChanged="rad_SelectedIndexChanged" DataSourceID="SqlDataSource1" GridLines="Both" GroupPanelPosition="Top">
        <ClientSettings>
            <Selecting AllowRowSelect="True"></Selecting>
        </ClientSettings>

        <MasterTableView DataSourceID="SqlDataSource1" AutoGenerateColumns="False">
            <Columns>
                <telerik:GridBoundColumn DataField="FLN" ReadOnly="True" HeaderText="نام و نام خانوادگی" SortExpression="FLN" UniqueName="FLN" DataType="System.Int64" FilterControlAltText="Filter FLN column"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ChildCount" HeaderText="تعداد اعضا" SortExpression="ChildCount" UniqueName="ChildCount" DataType="System.Int64" FilterControlAltText="Filter ChildCount column"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="PersonalID" HeaderText="PersonalID" SortExpression="PersonalID" UniqueName="PersonalID" DataType="System.Int64" FilterControlAltText="Filter PersonalID column" Display="False"></telerik:GridBoundColumn>


            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
                </section>
            </div>
            <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:cnnstring %>' ProviderName="System.Data.SqlClient" SelectCommand="SELECT *,Firstname+' '+Lastname as FLN FROM [InformationWithVamPardakhti2] where PersonalID<0 order by personalID DESC"></asp:SqlDataSource>
            <asp:SqlDataSource runat="server" ID="SqlDataSource2" ConnectionString='<%$ ConnectionStrings:cnnstring %>' ProviderName="System.Data.SqlClient" SelectCommand="SELECT *,Firstname+' '+Lastname as FLN FROM [tbl_information] WHERE (([PersonalID] > @PersonalID) AND ([ParentID] = @ParentID))">
                <SelectParameters>
                    <asp:Parameter DefaultValue="0" Name="PersonalID" Type="Int64"></asp:Parameter>
                    <asp:ControlParameter ControlID="temp" PropertyName="Text" Name="ParentID" Type="Int64"></asp:ControlParameter>
                </SelectParameters>

            </asp:SqlDataSource>


              <div class="col-lg-9" id="divUser" visible="false" runat="server">
                <section class="panel">
                    <header class="panel-heading">
                       نام کاربری و رمز عبور                          
                    </header>

                    </section>
                     <div class="input-group m-bot15" style="width: 250px">
                            <span class="input-group-addon">کد عضو</span>
                            <asp:TextBox  BackColor="White" ID="personalidUsertextbox"  Enabled="false" ReadOnly="true" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
                        </div>

                   <div class="input-group m-bot15" style="width: 250px">
                            <span class="input-group-addon">نام کاربری</span>
                            <asp:TextBox  BackColor="White" ID="UserTextBox"  CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
                        </div>
                         <div class="input-group m-bot15" style="width: 250px">
                            <span class="input-group-addon">رمز عبور</span>
                            <asp:TextBox  BackColor="White" ID="PasswordTextBox"  CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
                        </div>

                                                 <asp:Button ID="editUserpass" OnClick="editUserpass_Click"  runat="server" Enabled="true" CssClass="btn btn-info" Text="ثبت"   />

                  </div>


            <div class="col-lg-9" id="div1" visible="true" runat="server">
                <section class="panel">
                    <header class="panel-heading">
                        اعضای خانواده                           
                    </header>





                    <asp:TextBox runat="server" ID="temp" Visible="false"></asp:TextBox>
                    <div class="col-lg-6" id="DivEdit" visible="false" runat="server">
                                           <br />

                         <div class="input-group m-bot15" style="width: 250px">
                            <span class="input-group-addon">کد عضو</span>
                            <asp:TextBox  BackColor="White" ID="personalIDTextbox"  Enabled="false" ReadOnly="true" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
                        </div>
                         <div class="input-group m-bot15" style="width: 250px">
                            <span class="input-group-addon">نام پدر</span>
                            <asp:TextBox  BackColor="White" ID="fathernameTextBox"  CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
                        </div>
                         <div class="input-group m-bot15" style="width: 250px">
                            <span class="input-group-addon">تاریخ تولد</span>
                            <asp:TextBox  BackColor="White" ID="DOBTextbox"  CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
                        </div>
                       <div class="input-group m-bot15" style="width: 250px">
                            <span class="input-group-addon">کد ملی</span>
                            <asp:TextBox  BackColor="White" ID="mellicodeTextbox"  CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
                        </div>
                        


                               <asp:Button ID="editInformation"  OnClick="editInformation_Click" runat="server" Enabled="true" CssClass="btn btn-info" Text="ثبت"   />

                    </div>
                    <telerik:RadGrid OnItemCommand="RadGrid1_ItemCommand" ItemStyle-Font-Names="b yekan" ItemStyle-Font-Size="14px" AlternatingItemStyle-Font-Names="b yekan" AlternatingItemStyle-Font-Size="14px" HeaderStyle-Font-Names="b yekan" HeaderStyle-Font-Size="16px" ClientSettings-EnablePostBackOnRowClick="true" runat="server" ID="RadGrid1" CellSpacing="-1" GridLines="Both" GroupPanelPosition="Top" AutoGenerateColumns="False" DataSourceID="SqlDataSource2">
                        <ClientSettings>
                            <Selecting AllowRowSelect="True"></Selecting>
                        </ClientSettings>

                        <MasterTableView AutoGenerateColumns="false" DataSourceID="SqlDataSource2">
                            <Columns>
                                <telerik:GridBoundColumn DataField="FLN" ReadOnly="True" HeaderText="نام و نام خانوادگی" SortExpression="FLN" UniqueName="FLN" DataType="System.Int64" FilterControlAltText="Filter FLN column"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Active" HeaderText="فعال/غیرفعال" SortExpression="Active" UniqueName="Active" DataType="System.Int64" FilterControlAltText="Filter Active column"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="PersonalID" HeaderText="PersonalID" SortExpression="PersonalID" UniqueName="PersonalID" DataType="System.Int64" ReadOnly="true" FilterControlAltText="Filter PersonalID column" Display="False"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Fathername" HeaderText="نام پدر" SortExpression="Fathername" UniqueName="Fathername" FilterControlAltText="Filter Fathername column"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DOB" HeaderText="تاریخ تولد" SortExpression="DOB" UniqueName="DOB" FilterControlAltText="Filter DOB column"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Mellicode" HeaderText="کد ملی" SortExpression="Mellicode" UniqueName="Mellicode" FilterControlAltText="Filter Mellicode column"></telerik:GridBoundColumn>

                                                               <%-- <telerik:GridBoundColumn DataField="Username" HeaderText="نام کاربری" SortExpression="Username" UniqueName="Username" Display="false" FilterControlAltText="Filter Username column"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Password" HeaderText="رمز عبور" SortExpression="Password" UniqueName="Password" Display="false" FilterControlAltText="Filter Password column"></telerik:GridBoundColumn>--%>

                                <telerik:GridButtonColumn CommandName="Select" HeaderText="ویرایش" Text="ویرایش" UniqueName="Select">
                                </telerik:GridButtonColumn>

                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                </section>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
