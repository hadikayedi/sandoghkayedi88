﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SandoghKayedi.admin
{
    public partial class AppPage : System.Web.UI.Page
    {
        string Lastmonth, Lastyear, Lastmonthfarsi,mablagh;
        protected void GetMablagh()
        {
            using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("__GetPardakhtiMahaneFamilyWithCount", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@ParentID", SqlDbType.VarChar).Value = Request.QueryString["username"].ToString();
                    cmd.Parameters.Add("@date", SqlDbType.Int).Value = Lastyear+Lastmonth;

                    con.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            if (reader.Read())
                            {
                                mablagh = reader["SeparatePardakhti"].ToString();
                                Response.Write("<mablagh>" + mablagh + "</mablagh>");
                               
                            }
                        }
                    }
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["type"]==null || Session["type"].ToString() != "Admin")
            {
                Response.Redirect("/Login.aspx");
            }
            
            if ((Request.QueryString["username"] != null) && (Request.QueryString["password"] != null))
            {


                using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("__GetLastMonthYearNafarWithPlus", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@ParentID", SqlDbType.VarChar).Value = int.Parse(Request.QueryString["username"].ToString());
                        cmd.Parameters.Add("@Plusone", SqlDbType.Int).Value = 1;

                        con.Open();

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                if (reader.Read())
                                {
                                    Lastmonth = reader["Lastmonth"].ToString();
                                    
                                   
                                    Lastyear = reader["Lastyear"].ToString();
                                    Response.Write("<Lastyear>"+Lastyear+"</Lastyear>");
                                    Response.Write(Environment.NewLine);
                                    Lastmonthfarsi = reader["Lastmonthfarsi"].ToString();
                                    Response.Write("<Lastmonthfarsi>" + Lastmonthfarsi + "</Lastmonthfarsi>");
                                    Response.Write(Environment.NewLine);
                                    GetMablagh();
                                }
                            }
                        }
                    }
                }
            }


        }


    }
}