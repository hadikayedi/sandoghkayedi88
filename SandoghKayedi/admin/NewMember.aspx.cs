﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SandoghKayedi.admin
{
    public partial class NewMember : System.Web.UI.Page
    {
        protected int GetNewLoanTurn()
        {
            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.Text;


            cmd.CommandText = "select Max(LoanTurn) from tbl_Information  ";
            




            int NewLoanturn;
            conn.Open();
            SqlDataReader dr;
            dr = cmd.ExecuteReader();


            dr.Read();
            NewLoanturn =int.Parse( dr[0].ToString());
            conn.Close();
            return NewLoanturn+1;
        }
        protected int GetNewPersonalID()
        {
            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.Text;


            cmd.CommandText = "select Max(PersonalID) from tbl_Information  ";





            int NewPersonalID;
            conn.Open();
            SqlDataReader dr;
            dr = cmd.ExecuteReader();


            dr.Read();
            NewPersonalID = int.Parse(dr[0].ToString());
            conn.Close();
            return NewPersonalID + 1;
        }

        protected void SubmitNewMember(string name,string family , string fathername,string loanturn,string personalID,string parentID)
        {
            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.Text;


            cmd.CommandText = "insert into tbl_Information(PersonalID,ParentID,Firstname,Lastname,Loanturn,Fathername,Active) values ('" + personalID + "','" + parentID + "','" + name + "','" + family + "','" + loanturn + "','" + fathername + "','1')  ";


            conn.Open();
           
             cmd.ExecuteNonQuery();

            conn.Close();
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["type"]==null || Session["type"].ToString() != "Admin")
            {
                Response.Redirect("/Login.aspx");
            }
            loanturn.Text = GetNewLoanTurn().ToString();
            

        }

        protected void SubmitFamily_Click(object sender, EventArgs e)
        {
            string family = lastname.Text + "(" + count.Text + ")";
            SubmitNewMember(firstname.Text, family, fathername.Text, loanturn.Text, GetNewPersonalID().ToString(), parent.SelectedValue.ToString());
            Radgrid.DataBind();
            GreenMessagebox.Visible = true;
            firstname.Text = "";
            lastname.Text = "";
            fathername.Text = "";
            loanturn.Text = GetNewLoanTurn().ToString();
            count.Text = "";
        }
    }
}