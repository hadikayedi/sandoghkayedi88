﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ad.Master" AutoEventWireup="true" CodeBehind="NewUserSubmit.aspx.cs" Inherits="SandoghKayedi.admin.NewUserSubmit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <script src="../script/PersianDatePicker.js"></script>
    <script src="../script/PersianDatePicker.min.js"></script>
    <link href="../Content/PersianDatePicker.min.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        div.rcbSlide .RadComboBoxDropDown .rcbScroll
       {
 		font-family: 'B Yekan';
         font-size:14px;
    }

    </style>
    
        <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:cnnstring %>' ProviderName="System.Data.SqlClient" SelectCommand="GetMonthYearFromBegin" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="parent" PropertyName="SelectedValue" Name="UserID" Type="Int32"></asp:ControlParameter>
            </SelectParameters>
        </asp:SqlDataSource>

    <asp:ScriptManager runat="server"></asp:ScriptManager>
<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <div class="col-lg-6" id="divselect" visible="true"  runat="server"  >
                        <section class="panel">
                            <header class="panel-heading">
ماه ها
                            </header>
                             <div class="input-group m-bot15" style="width:300px" >
                                <span class="input-group-addon">فرد مورد نظر</span>
                            <telerik:RadComboBox runat="server"  CssClass="form-control blacktextbox" Font-Size="14px"  Font-Names="B yekan" EmptyMessage="انتخاب نمایید" AllowCustomText="true" Filter="Contains"  AutoPostBack="true"   ID="parent" DataSourceID="SqlDataSource2" DataTextField="FLN" DataValueField="PersonalID" OnSelectedIndexChanged="parent_SelectedIndexChanged"></telerik:RadComboBox>
                           </div>
                                  <asp:SqlDataSource runat="server" ID="SqlDataSource2" ConnectionString='<%$ ConnectionStrings:cnnstring %>' ProviderName="System.Data.SqlClient" SelectCommand="SELECT [Firstname]+' '+ [Lastname] as FLN, [PersonalID] FROM [NewUserSubmit]"></asp:SqlDataSource>
                            <br />
                            <asp:CheckBoxList  RepeatLayout="Table" RepeatColumns="5" RepeatDirection="Vertical" CellPadding="4"  ID="CheckBoxList1" Font-Names="B yekan"  OnSelectedIndexChanged="CheckBoxList1_SelectedIndexChanged"  runat="server" AutoPostBack="True" DataSourceID="SqlDataSource1" DataTextField="monthyearfarsi" DataValueField="monthyear" >
    </asp:CheckBoxList>
                            </section>
            </div>
        <div class="col-lg-6 " id="div1" visible="true"   runat="server"  >
                        <section class="panel">
                            <header class="panel-heading">
مبلغ

                            </header>
                                                   <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="FamilySubmit" ForeColor="Red" runat="server" DisplayMode="BulletList" ShowMessageBox="False" EnableClientScript="True" Font-Strikeout="False" />

                              <div class="input-group m-bot15" style="width:350px" >
                                <span class="input-group-addon">آخرین ماه</span>
                                  <asp:Label   runat="server" ForeColor="Black" Enabled="false" BackColor="White" Font-Names="b yekan" CssClass="form-control blacktextbox"  ID="monthyear"></asp:Label>
</div>
                            <div class="input-group m-bot15" style="width:350px" >
                                <span class="input-group-addon">مجموع پرداخت ها تا کنون (ریال)</span>
                                    <asp:TextBox runat="server" ForeColor="Black" Enabled="false" BackColor="White" Font-Names="b yekan" CssClass="form-control blacktextbox" AutoPostBack="true" ID="SumPardakhti"  ></asp:TextBox>
</div>
<div class="input-group m-bot15" style="width:250px" >
                                <span class="input-group-addon">نوع پرداخت</span>
                <telerik:RadComboBox  CssClass="form-control blacktextbox" Font-Size="14px"  Font-Names="B yekan"     ID="Paytype" runat="server"  >
                    <Items>
                        <telerik:RadComboBoxItem runat="server" Text="پرداخت اینترنتی" Value="پرداخت اینترنتی"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="پرداخت نقدی" Value="پرداخت نقدی"></telerik:RadComboBoxItem>
                    </Items>
                </telerik:RadComboBox>
                 </div>
                                    <div class="input-group m-bot15" style="width:250px" >
                                <span class="input-group-addon">تاریخ</span>
                                         
            <asp:TextBox  ID="datepicker" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator DisplayMode="BulletList" Display="None" ControlToValidate="datepicker" ID="RequiredFieldValidator1" ValidationGroup="FamilySubmit" runat="server" ForeColor="Red" ErrorMessage="تاریخ را وارد نمایید"></asp:RequiredFieldValidator>
                                       
                 </div>

                                    <div class="input-group m-bot15" style="width:250px" >
                                <span class="input-group-addon">شماره فیش</span>
            <asp:TextBox  ID="fish" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator DisplayMode="BulletList" Display="None" ControlToValidate="fish" ID="RequiredFieldValidator2" ValidationGroup="FamilySubmit" runat="server" ForeColor="Red" ErrorMessage="شماره فیش را وارد نمایید"></asp:RequiredFieldValidator>

                 </div>
                                    <div class="input-group m-bot15" style="width:250px" >
                                <span class="input-group-addon">توضیحات</span>
            <asp:TextBox  ID="desc" TextMode="MultiLine" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan"  runat="server"  ></asp:TextBox>
                 </div>
                              <div class="input-group m-bot15" style="width:250px" >
                                <span class="input-group-addon">مبلغ انتخاب شده (ریال)</span>
        <asp:Label runat="server" ID="hadi" BackColor="YellowGreen" ForeColor="Black" Font-Names="b yekan" CssClass="form-control blacktextbox" Text="0"></asp:Label>
</div>
                                                        <asp:Button Text="ثبت" CssClass="btn btn-info" ValidationGroup="FamilySubmit"  runat="server" ID="btnnew" OnClick="btnnew_Click"/>

                            </section>
            </div>


    </ContentTemplate>
</asp:UpdatePanel>
    
</asp:Content>
