﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ad.Master" AutoEventWireup="true" CodeBehind="NewMember.aspx.cs" Inherits="SandoghKayedi.admin.NewMember" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" ChildrenAsTriggers="true" runat="server">

        <ContentTemplate>

            <div class="col-lg-6">
                <section class="panel">
                    <header class="panel-heading">
                        افزودن عضو و خانواده جدید     
                    </header>
                    <div class="panel-body">
                        <asp:RadioButton runat="server" AutoPostBack="true" Checked="true" ID="NewMember1" Text="  افزودن عضو جدید به خانواده موجود  " GroupName="ozv" />

                        <asp:RadioButton runat="server" Visible="false" AutoPostBack="true" ID="NewFamily1" Text="افزودن عضو جدید به همراه خانواده جدید" GroupName="ozv" />

                        <asp:DropDownList runat="server" ID="parent"  AutoPostBack="true" DataSourceID="SqlDataSource1" DataTextField="FLN" DataValueField="PersonalID">
                        </asp:DropDownList>


                        <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:cnnstring %>' SelectCommand="select *,Firstname+' '+Lastname as FLN from tbl_Information where PersonalID<0 and Active=1"></asp:SqlDataSource>
                        <br /><br />
                        <div class="input-group m-bot15" style="width: 250px">
                            <span class="input-group-addon">نام</span>
                            <asp:TextBox  BackColor="White" ID="firstname"  CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
                        </div>
                         <div class="input-group m-bot15" style="width: 250px">
                            <span class="input-group-addon">نام خانوادگی</span>
                        <asp:TextBox  BackColor="White" ID="lastname" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>

                        </div>
                         <div class="input-group m-bot15" style="width: 250px">
                            <span class="input-group-addon">شماره مورد نظر</span>
                            <asp:TextBox  BackColor="White" ID="count" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
                        </div>
                        <div class="input-group m-bot15" style="width: 250px">
                            <span class="input-group-addon">نوبت دریافت وام</span>
                            <asp:TextBox ReadOnly="true"  BackColor="White" ID="loanturn" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
                        </div>
                        <div class="input-group m-bot15" style="width: 250px">
                            <span class="input-group-addon">نام پدر</span>
                            <asp:TextBox  BackColor="White" ID="fathername" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
                        </div>
                         <asp:Button ID="SubmitFamily" runat="server" Enabled="true" CssClass="btn btn-info" Text="افزودن"  OnClick="SubmitFamily_Click" />

                        <div runat="server" id="GreenMessagebox" visible="false" style="background-color:lightgreen;margin-top:20px;width:300px;margin:0px auto;height:40px;text-align:center;border-radius:20px">
                                        عضو مورد نظر با موفقیت افزوده شد
                                        <br />
                                        <br />
                                        <br />


            </div>

                    </div>
                </section>
            </div>
            <div class="col-lg-6">
                <section class="panel">
                    <header class="panel-heading">
                        عضو های خانواده     
                    </header>
                    <div class="panel-body">
                      
                        <telerik:RadGrid runat="server" AlternatingItemStyle-Font-Names="B yekan" HeaderStyle-Font-Names="b yekan" ItemStyle-Font-Names="B yekan" CellSpacing="-1" DataSourceID="SqlDataSource2" GridLines="Both" GroupPanelPosition="Top" ID="Radgrid">
                            <MasterTableView DataSourceID="SqlDataSource2" AutoGenerateColumns="False">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="PersonalID" HeaderText="شماره عضویت" SortExpression="PersonalID" UniqueName="PersonalID" DataType="System.Int64" FilterControlAltText="Filter PersonalID column"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FLN" HeaderText="نام و نام خانوادگی" SortExpression="FLN" UniqueName="FLN" FilterControlAltText="Filter FLN column"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Loanturn" HeaderText="نوبت وام" SortExpression="Loanturn" UniqueName="Loanturn" DataType="System.Int64" FilterControlAltText="Filter Loanturn column"></telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Active" HeaderText="فعال" SortExpression="Active" UniqueName="Active" DataType="System.Int64" FilterControlAltText="Filter Active column"></telerik:GridBoundColumn>


                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>

                        <asp:SqlDataSource runat="server" ID="SqlDataSource2" ConnectionString='<%$ ConnectionStrings:cnnstring %>' SelectCommand="SELECT *,Firstname+' '+Lastname as FLN FROM [tbl_Information] WHERE ([ParentID] = @ParentID) order by Loanturn">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="parent" PropertyName="SelectedValue" Name="ParentID" Type="Int64"></asp:ControlParameter>
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </div>
                </section>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
