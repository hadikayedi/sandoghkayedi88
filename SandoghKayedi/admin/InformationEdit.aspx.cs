﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace SandoghKayedi.admin
{
    public partial class InformationEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["type"] == null || Session["type"].ToString() != "Admin")
            {
                Response.Redirect("/Login.aspx");
            }
        }


        protected void rad_SelectedIndexChanged(object sender, EventArgs e)
        {

            foreach (GridDataItem item in rad.SelectedItems)
            {
                temp.Text = item["PersonalID"].Text;

            }
            FillUSerPass();

        }




        protected void FillUSerPass()
        {

            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString;
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "select top 1 username,password,personalid from tbl_information where personalId=@personalid";
                    command.Parameters.AddWithValue("personalid",temp.Text);
              
                    connection.Open();

                   SqlDataReader dr= command.ExecuteReader();
                    while(dr.Read())
                    {
                        personalidUsertextbox.Text = dr["personalid"].ToString();
                        UserTextBox.Text = dr["Username"].ToString();
                        PasswordTextBox.Text = dr["Password"].ToString() ;
                    }
                }
            }
           
            divUser.Visible = true;
        }

        protected void UpdateUserPass()
        {

            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString;
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "update tbl_information set username=@username , password=@password where personalId=@personalid";
                    command.Parameters.AddWithValue("username", UserTextBox.Text);
                    command.Parameters.AddWithValue("password", PasswordTextBox.Text);
                    command.Parameters.AddWithValue("personalid", personalidUsertextbox.Text);


                    connection.Open();

                    command.ExecuteNonQuery();
                }
            }

        }
        protected void updateuserinformation()
        {

            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString;
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "update tbl_information set fathername=@fathername,DOB=@dob,Mellicode=@mellicode where personalId=@personalid";
                    command.Parameters.AddWithValue("fathername", fathernameTextBox.Text);
                    command.Parameters.AddWithValue("dob", DOBTextbox.Text);
                    command.Parameters.AddWithValue("mellicode", mellicodeTextbox.Text);
                    command.Parameters.AddWithValue("personalid", personalIDTextbox.Text);


                    connection.Open();

                    command.ExecuteNonQuery();
                }
            }

        }



        protected void RadGrid1_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                GridDataItem data = (GridDataItem)e.Item;
                mellicodeTextbox.Text = data["Mellicode"].Text == "&nbsp;" ? "" : data["Mellicode"].Text;
                fathernameTextBox.Text = data["Fathername"].Text == "&nbsp;" ? "" : data["Fathername"].Text;
                DOBTextbox.Text = data["DOB"].Text == "&nbsp;" ? "" : data["DOB"].Text;
                personalIDTextbox.Text = data["PersonalID"].Text == "&nbsp;" ? "" : data["PersonalID"].Text;

                DivEdit.Visible = true;
            }
        }

        protected void editUserpass_Click(object sender, EventArgs e)
        {
            UpdateUserPass();
           
        }

        protected void editInformation_Click(object sender, EventArgs e)
        {
            updateuserinformation();
            DivEdit.Visible = false;
            RadGrid1.DataBind();
        }






    }
}