﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ad.Master" AutoEventWireup="true" CodeBehind="Information.aspx.cs" Inherits="RSTAS.admin.AddNew" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .Treeviewheader {
            font-family: "B Yekan";
            font-size: 14px;
            font-weight: bold;
        }

        .Treeviewitem {
            font-family: "B Yekan";
            font-size: 13px;
        }

        .Treeviewalter {
            font-family: "B Yekan";
            font-size: 13px;
        }
        .remainColor {
            color: red;
            font-weight: bold;

        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .byekan {
            font-family: 'B Yekan';
        }
    </style>
    <div class="col-lg-12" id="divselect" visible="true" runat="server">
        <section class="panel">
            <header class="panel-heading">
                اطلاعات کل اعضا

            </header>
            <div class="row state-overview">
                <div class="col-lg-2 col-sm-6 ">
                    <section class="panel">
                        <div class="symbol" style="background-color: green">
                            <i class="icon-user"></i>
                        </div>
                        <div class="value">
                            <h1 class="byekan" style="color: green">
                                <asp:Label runat="server" ID="ActiveUserCount">20</asp:Label></h1>
                            <p style="font-size: 20px; color: green">عضو فعال</p>
                        </div>
                    </section>
                </div>
                <div class="col-lg-2 col-sm-6">
                    <section class="panel">
                        <div class="symbol " style="background-color: #f1d438">
                            <i class="icon-home"></i>
                        </div>
                        <div class="value">
                            <h1 class="byekan" style="color: #f1d438">
                                <asp:Label runat="server" ID="ActiveFamilyCount">20</asp:Label></h1>
                            <p style="font-size: 20px; color: #f1d438">خانواده فعال</p>
                        </div>
                    </section>
                </div>
                <div class="col-lg-2 col-sm-6">
                    <section class="panel">
                        <div class="symbol " style="background-color: #ff7d00">
                            <i class="icon-dollar"></i>
                        </div>
                        <div class="value">
                            <h1 class="byekan" style="color: #ff7d00">
                                <asp:Label runat="server" ID="VamCount">20</asp:Label></h1>
                            <p style="font-size: 16px; color: #ff7d00">مورد وام پرداختی</p>
                        </div>
                    </section>
                </div>
                <div class="col-lg-2 col-sm-6">
                    <section class="panel">
                        <div class="symbol" style="background-color: #0072ff">
                            <i class="icon-time"></i>
                        </div>
                        <div class="value">
                            <h1 class="byekan" style="color: blue">
                                <asp:Label runat="server" ID="FromBegin">20</asp:Label></h1>
                            <p style="font-size: 14px; color: blue">ماه از شروع صندوق</p>
                        </div>
                    </section>
                </div>
                <div class="col-lg-2 col-sm-6">
                    <section class="panel">
                        <div class="symbol red">
                            <i class="icon-user"></i>
                        </div>
                        <div class="value">
                            <h1 class="byekan" style="color: red">
                                <asp:Label runat="server" ID="DeactiveUserCount">20</asp:Label></h1>
                            <p style="font-size: 20px; color: red">عضو غیر فعال</p>
                        </div>
                    </section>
                </div>
                <div class="col-lg-2 col-sm-6">
                    <section class="panel">
                        <div class="symbol red">
                            <i class="icon-home"></i>
                        </div>
                        <div class="value">
                            <h1 class="byekan" style="color: red">
                                <asp:Label runat="server" ID="DeactiveFamilyCount">20</asp:Label></h1>
                            <p style="font-size: 18px; color: red">خانواده غیر فعال</p>
                        </div>
                    </section>
                </div>
            </div>
            <telerik:RadScriptManager ID="scrpt" runat="server"></telerik:RadScriptManager>
            <telerik:RadTreeList Dir="RTL" ID="InformationGrid"  HeaderStyle-CssClass="Treeviewheader" ItemStyle-CssClass="Treeviewitem" AlternatingItemStyle-CssClass="Treeviewalter" runat="server" DataSourceID="SqlDataSource1"
                ParentDataKeyNames="ParentID" OnItemDataBound="InformationGrid_OnItemDataBound" DataKeyNames="PersonalID" ExpandCollapseMode="Client" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="false" Skin="Web20">

                <ClientSettings>
                    <Selecting AllowItemSelection="True" AllowToggleSelection="True"></Selecting>
                </ClientSettings>

                <SortingSettings SortToolTip="برای مرتب سازی کلیک بفرمایید" SortedAscToolTip="صعودی" SortedDescToolTip="نزولی"></SortingSettings>

                <Columns>
                    
                    <telerik:TreeListTemplateColumn HeaderText="اسامی نفرات">
                        <ItemTemplate>
                            <asp:Label runat="server"><%# Eval("FLN") %></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right"/>
                        <HeaderStyle Width="200px" />
                    </telerik:TreeListTemplateColumn>


                    <%--<telerik:TreeListBoundColumn DataField="FLN"  UniqueName="Information_ID" HeaderText="اسامی نفرات">
                    </telerik:TreeListBoundColumn>--%>

                    <telerik:TreeListBoundColumn DataField="Loanturn" UniqueName="Information_ID" HeaderText="نوبت وام ها">
                    </telerik:TreeListBoundColumn>
                    <telerik:TreeListBoundColumn DataField="ChildCount" UniqueName="Information_ID" HeaderText="تعداد عضوها">
                    </telerik:TreeListBoundColumn>
                    <telerik:TreeListBoundColumn DataField="CountVam" UniqueName="Information_ID" HeaderText="تعداد وام های دریافتی">
                    </telerik:TreeListBoundColumn>
                    <telerik:TreeListBoundColumn DataField="Sumvam" UniqueName="Information_ID" HeaderText="مجموع مبالغ وام ها (ریال)">
                    </telerik:TreeListBoundColumn>
                    <telerik:TreeListBoundColumn DataField="SumOzviat" UniqueName="Information_ID" HeaderText="مجموع مبالغ حق عضویت (ریال) ">
                    </telerik:TreeListBoundColumn>
                    <telerik:TreeListBoundColumn DataField="SumBazpardakht" UniqueName="Information_ID" HeaderText="مجموع مبالغ پرداختی ها (ریال)">
                    </telerik:TreeListBoundColumn>
                    <telerik:TreeListBoundColumn DataField="Remain" ItemStyle-CssClass="remainColor" UniqueName="Remain" HeaderText=" مبالغ پرداخت نشده (ریال)">
                    </telerik:TreeListBoundColumn>
                    <telerik:TreeListBoundColumn DataField="Lastmonthyear" UniqueName="Information_ID" HeaderText="آخرین واریزی">
                    </telerik:TreeListBoundColumn>
                    <telerik:TreeListBoundColumn DataField="RemainMonthYearPersian" UniqueName="RemainMonthYearPersian" HeaderText="آخرین ماه بازپرداخت وام">
                    </telerik:TreeListBoundColumn>









                </Columns>
            </telerik:RadTreeList>
            <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:cnnstring %>' SelectCommand="SELECT *,Firstname+' '+Lastname as FLN,Lastmonth+' '+Lastyear as Lastmonthyear FROM InformationWithVamPardakhti2 order by Information_ID asc "></asp:SqlDataSource>
        </section>
    </div>

</asp:Content>
