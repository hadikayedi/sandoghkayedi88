﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ad.Master" AutoEventWireup="true" CodeBehind="PardakhtSummary.aspx.cs" Inherits="SandoghKayedi.admin.PardakhtSummary" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <style>
        .Treeviewheader{
            font-family:"B Yekan";
            font-size:14px;
            font-weight:bold;

        }
        .Treeviewitem{
            font-family:"B Yekan";
            font-size:13px;

        }
        .Treeviewalter{
            font-family:"B Yekan";
            font-size:13px;


        }
        div.rcbSlide .RadComboBoxDropDown .rcbScroll
       {
 		font-family: 'B Yekan';
         font-size:14px;
    }
    </style>



     <asp:ScriptManager runat="server"></asp:ScriptManager>
    

<asp:UpdatePanel runat="server" ID="UpdatePanel1">
    <ContentTemplate>
        <div class="col-lg-4" id="divselect" visible="true"  runat="server"  >
                        <section class="panel">
                            <header class="panel-heading">
جست و جو
                            </header>
                            
                             <div class="input-group m-bot15" style="width:300px" >
                                <span class="input-group-addon"><asp:RadioButton ID="byUser" AutoPostBack="true" GroupName="t" Checked="true" runat="server" OnCheckedChanged="byUser_CheckedChanged" />
                                    بر اساس نفر
                                </span>
                                 <telerik:RadComboBox runat="server" CssClass="form-control blacktextbox"  Font-Size="14px" Font-Names="B yekan" EmptyMessage="انتخاب نمایید" AllowCustomText="true" Filter="Contains" AutoPostBack="true" ID="User" DataSourceID="SqlDataSource1" DataTextField="FLN" DataValueField="PersonalID" OnSelectedIndexChanged="User_SelectedIndexChanged"></telerik:RadComboBox>
                                 <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:cnnstring %>' ProviderName="System.Data.SqlClient" SelectCommand="SELECT [PersonalID], [Firstname]+' '+ [Lastname] as FLN FROM [tbl_Information] where PersonalID>0"></asp:SqlDataSource>
                             </div>
                             <div class="input-group m-bot15" style="width:300px" >
                                <span class="input-group-addon"><asp:RadioButton ID="byParent" AutoPostBack="true" GroupName="t"  runat="server" OnCheckedChanged="byParent_CheckedChanged"/>
                                    بر اساس خانواده</span>
                                 <telerik:RadComboBox runat="server" CssClass="form-control blacktextbox" Font-Size="14px" Font-Names="B yekan" EmptyMessage="انتخاب نمایید" AllowCustomText="true" Filter="Contains" AutoPostBack="true" ID="Parent" DataSourceID="SqlDataSource2" DataTextField="FLN" DataValueField="PersonalID" OnSelectedIndexChanged="Parent_SelectedIndexChanged"></telerik:RadComboBox>
                                 <asp:SqlDataSource runat="server" ID="SqlDataSource2" ConnectionString='<%$ ConnectionStrings:cnnstring %>' ProviderName="System.Data.SqlClient" SelectCommand="SELECT [PersonalID], [Firstname]+' '+ [Lastname] as FLN FROM [tbl_Information] where PersonalID<0"></asp:SqlDataSource>
                             </div>
                             <div class="input-group m-bot15" style="width:350px" >
                                <span class="input-group-addon"><asp:RadioButton ID="byMonthYear" AutoPostBack="true" GroupName="t"  runat="server" OnCheckedChanged="byMonthYear_CheckedChanged" />
                                    بر اساس ماه</span>
                                 <telerik:RadComboBox runat="server" CssClass="form-control blacktextbox" Font-Size="14px" Font-Names="B yekan" EmptyMessage="انتخاب نمایید" AllowCustomText="true" Filter="Contains" AutoPostBack="true" ID="mah"  >
                                     <Items>
                                         <telerik:RadComboBoxItem runat="server" Text="فروردین" Value="01"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="اردیبهشت" Value="02"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="خرداد" Value="03"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="تیر" Value="04"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="مرداد" Value="05"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="شهریور" Value="06"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="مهر" Value="07"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="آبان" Value="08"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="آذر" Value="09"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="دی" Value="10"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="بهمن" Value="11"></telerik:RadComboBoxItem>
                                         <telerik:RadComboBoxItem runat="server" Text="اسفند" Value="12"></telerik:RadComboBoxItem>
                                     </Items>
                                 </telerik:RadComboBox>
                                 <telerik:RadComboBox runat="server" CssClass="form-control blacktextbox" Font-Size="14px" Font-Names="B yekan" EmptyMessage="انتخاب نمایید" AllowCustomText="true" Filter="Contains" AutoPostBack="true" ID="sal" DataSourceID="SqlDataSource3" DataTextField="Yearr" DataValueField="Yearr" OnSelectedIndexChanged="sal_SelectedIndexChanged">
                                 </telerik:RadComboBox>
                                 <asp:SqlDataSource runat="server" ID="SqlDataSource3" ConnectionString='<%$ ConnectionStrings:cnnstring %>' ProviderName="System.Data.SqlClient" SelectCommand="SELECT Distinct Left([monthyear],4) as Yearr FROM [pardakhti] Order by Yearr"></asp:SqlDataSource>
                             </div>
                            
 <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                <ProgressTemplate>
                                   <label runat="server" style="color:red;margin:0px auto"> در حال بارگزاری</label>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                           <asp:Button Text="پاک کردن جست و جو" CssClass="btn btn-info"   runat="server" ID="btnShow" OnClick="btnShow_Click" />

                            
                            <asp:TextBox runat="server" ID="byWhat" Text="ssid" Visible="false"></asp:TextBox>
                            
                            <asp:TextBox runat="server" ID="What" Visible="false" ></asp:TextBox>
                            </section>
            </div>
         <div class="col-lg-8" id="div1" visible="true"  runat="server"  >
                        <section class="panel">
                            <header class="panel-heading">
نتایج پرداختی ها
                            </header>
                            <telerik:RadGrid runat="server"  HeaderStyle-CssClass="Treeviewheader" ItemStyle-CssClass="Treeviewitem" AlternatingItemStyle-CssClass="Treeviewalter" ID="vamview" CellSpacing="-1" GridLines="Both" GroupPanelPosition="Top" ShowGroupPanel="True">



                                <ClientSettings AllowDragToGroup="True">
                                    <Selecting AllowRowSelect="True"></Selecting>
                                </ClientSettings>
                                
                                <MasterTableView  AutoGenerateColumns="False" GroupsDefaultExpanded="false" GroupHeaderItemStyle-CssClass="Treeviewitem">
                                      <GroupByExpressions> 
            <telerik:GridGroupByExpression> 
                 <GroupByFields> 

                    <telerik:GridGroupByField FieldName="ParentName"  HeaderText="خانواده" /> 

                </GroupByFields> 
                <SelectFields> 
                    <telerik:GridGroupByField FieldName="ParentName" HeaderText="خانواده" /> 
                </SelectFields> 
         
            </telerik:GridGroupByExpression> 
                                           <telerik:GridGroupByExpression> 
                 <GroupByFields> 

                    <telerik:GridGroupByField FieldName="FLN"  HeaderText="نام و نام خانوادگی" /> 
                </GroupByFields> 
                <SelectFields> 
                    <telerik:GridGroupByField FieldName="FLN" HeaderText="نام و نام خانوادگی" /> 
                </SelectFields> 
            </telerik:GridGroupByExpression> 
        </GroupByExpressions>        
                                    <NoRecordsTemplate >
                                       <center>
                                           موردی جهت نمایش یافت نشد
                                       </center>
                                        
                                    </NoRecordsTemplate>
                                     <Columns> 
                                                <telerik:GridBoundColumn  DataField="FLN" HeaderText="نام و نام خانوادگی" ItemStyle-Width="100px"  SortExpression="FLN" UniqueName="FLN" FilterControlAltText="Filter FLN column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="typepay" HeaderText="نوع پرداخت" SortExpression="vam" UniqueName="vam" FilterControlAltText="Filter vam column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="month" HeaderText="ماه" SortExpression="month" UniqueName="month" FilterControlAltText="Filter month column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="year" HeaderText="سال" SortExpression="year" UniqueName="year" DataType="System.Int64" FilterControlAltText="Filter year column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="fish" HeaderText="شماره فیش" SortExpression="fish" UniqueName="fish" FilterControlAltText="Filter marhale column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="FishDate" HeaderText="تاریخ فیش" SortExpression="FishDate" UniqueName="FishDate" FilterControlAltText="Filter fish column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="ozviat" HeaderText="حق عضویت" SortExpression="ozviat" UniqueName="ozviat" FilterControlAltText="Filter fish column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="bazpardakht" HeaderText="بازپرداخت وام" SortExpression="bazpardakht" UniqueName="bazpardakht" FilterControlAltText="Filter fish column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="plus" HeaderText="جمع پرداختی" SortExpression="plus" UniqueName="plus" FilterControlAltText="Filter fish column"></telerik:GridBoundColumn>


                                                <telerik:GridBoundColumn DataField="tozihat" HeaderText="توضیحات" SortExpression="tozihat" UniqueName="tozihat" FilterControlAltText="Filter tozihat column"></telerik:GridBoundColumn>
                                            </Columns>
                                        </MasterTableView>
                            </telerik:RadGrid>

                          
                        </section>
             </div>
        </ContentTemplate>

    </asp:UpdatePanel>
</asp:Content>