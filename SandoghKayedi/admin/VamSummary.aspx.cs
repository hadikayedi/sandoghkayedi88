﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

using System.Drawing;

namespace SandoghKayedi.admin
{
    public partial class VamSummary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["type"]==null || Session["type"].ToString() != "Admin")
            {
                Response.Redirect("/Login.aspx");
            }
            if (!Page.IsPostBack)
            {
                User.Enabled = true;
                Parent.Enabled = false;
                mah.Enabled = false;
                sal.Enabled = false;
                dore.Enabled = false;
                byWhat.Text = "ssid";
                
            }
        }

        protected void byUser_CheckedChanged(object sender, EventArgs e)
        {
            User.Enabled = true;
            Parent.Enabled = false;
            mah.Enabled = false;
            sal.Enabled = false;
            dore.Enabled = false;
            byWhat.Text = "ssid";
            User.ClearSelection();
            User.Text = string.Empty;
        }

        protected void byParent_CheckedChanged(object sender, EventArgs e)
        {
            User.Enabled = false;
            Parent.Enabled = true;
            mah.Enabled = false;
            sal.Enabled = false;
            dore.Enabled = false;
            byWhat.Text = "ssidf";
            Parent.ClearSelection();
            Parent.Text = string.Empty;

        }

        protected void byMonthYear_CheckedChanged(object sender, EventArgs e)
        {
            User.Enabled = false;
            Parent.Enabled = false;
            mah.Enabled = true;
            sal.Enabled = true;
            dore.Enabled = false;
            byWhat.Text = "monthyear";
            mah.ClearSelection();
            mah.Text = string.Empty;
            sal.ClearSelection();
            sal.Text = string.Empty;


        }

        protected void byIDvam_CheckedChanged(object sender, EventArgs e)
        {
            User.Enabled = false;
            Parent.Enabled = false;
            mah.Enabled = false;
            sal.Enabled = false;
            dore.Enabled = true;
            byWhat.Text = "marhale";
            dore.ClearSelection();
            dore.Text = string.Empty;

        }

        protected void VamGenerate()
        {
            vamview.DataSource = null;

            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "select vam,Left(monthyear,4) as year,right(monthyear,2) as month,marhale,fish,tozihat,Firstname+' '+Lastname as FLN from VamWithNames where " + byWhat.Text + "='" + What.Text + "' ";
            SqlDataReader dr;
            conn.Open();
            dr=cmd.ExecuteReader();
            vamview.DataSource=dr;
            vamview.DataBind();
            conn.Close();
            

        }
        protected void User_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            What.Text = User.SelectedValue.ToString();
            VamGenerate();

        }

        protected void Parent_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            What.Text = Parent.SelectedValue.ToString();
            VamGenerate();

        }

        protected void sal_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            What.Text = sal.SelectedValue.ToString() + mah.SelectedValue.ToString();
            VamGenerate();
            

        }

        protected void dore_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            What.Text = dore.SelectedValue.ToString();
            VamGenerate();

           
        }
        protected void btnShow_Click(object sender, EventArgs e)
        {
            User.ClearSelection();
            User.Text = string.Empty;
            Parent.ClearSelection();
            Parent.Text = string.Empty;
            mah.ClearSelection();
            mah.Text = string.Empty;
            sal.ClearSelection();
            sal.Text = string.Empty;
            dore.ClearSelection();
            dore.Text = string.Empty;

        }




    
    }
}