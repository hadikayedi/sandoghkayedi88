﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;
using System.Drawing;

namespace SandoghKayedi
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string text = (Guid.NewGuid().ToString()).Substring(0, 5);
            Response.Cookies["Captcha"]["value"] = text;
            imagecaptcha.ImageUrl = "Captcha.aspx";
        }
        public static String left(String input, int len)
        {
            return input.Substring(0, len);
        }

        public static String right(String input, int len)
        {
            return input.Substring(input.Length - len);
        }

        public static String mid(String input, int index, int len)
        {
            return input.Substring(index - 1, index + len - 1);
        }

        public static String mid(String input, int index)
        {
            return input.Substring(index - 1);
        }
       

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (TextBox1.Text != Request.Cookies["Captcha"]["value"])
            {
                Label1.Text = "متن وارد شده از تصویر امنیتی اشتباه می باشد";
                Label1.Visible = true;
            }
            else
            {
                var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = con.ConnectionString.ToString();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = System.Data.CommandType.Text;


                cmd.CommandText = "select * from tbl_Information where [Username]='" + user.Text + "' and [Password]='" + pass.Text + "' ";
                SqlDataReader dr;
                conn.Open();
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    dr.Read();
                    if (dr["Active"].ToString() == "0")
                    {
                        Label1.Text = "کاربر مورد نظر توسط مدیریت مسدود شده است";
                        Label1.Visible = true;
                    }
                    else if (dr["Admin"].ToString() == "1")
                    {

                        HttpContext.Current.Session.Add("user", user.Text);
                        string tmp = dr["Firstname"].ToString() + " " + dr["Lastname"].ToString();
                        HttpContext.Current.Session.Add("FLn", tmp);
                        HttpContext.Current.Session.Add("type", "Admin");
                        HttpContext.Current.Session.Add("personalID", dr["PersonalID"].ToString());
                        //HttpContext.Current.Session.Add("Access", dr["Access"].ToString());
                        //HttpContext.Current.Session.Add("Categories", dr["Categories"].ToString());
                       
                        


                        Response.Redirect("/admin");
                    }
                    else
                    {
                        HttpContext.Current.Session.Add("user", user.Text);
                        string tmp = dr["FirstName"].ToString() + " " + dr["LastName"].ToString();
                        HttpContext.Current.Session.Add("FLn", tmp);
                        HttpContext.Current.Session.Add("personalID", dr["PersonalID"].ToString());

                        HttpContext.Current.Session.Add("type", "User");

                        Response.Redirect("/user");
                    }
                   
                }
                else if (!dr.HasRows)
                {
                    Label1.Text = "نام کاربری یا رمز عبور اشتباه می باشد";
                    Label1.Visible = true;
                }
                conn.Close();
            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            
                Response.Cookies["Captcha"]["value"] = (Guid.NewGuid().ToString()).Substring(0, 5);
                imagecaptcha.ImageUrl = "Captcha.aspx";
            


        }
    }
}