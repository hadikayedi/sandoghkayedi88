﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SandoghKayedi.user
{
    public partial class Profile : System.Web.UI.Page
    {
        protected string GetPersonalID(string Username)
        {

            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "select * from tbl_Information where Username='" + Username + "'";

            SqlDataReader dr;
            conn.Open();
            dr = cmd.ExecuteReader();
            dr.Read();
            string res = dr["PersonalID"].ToString();
            Session["personalId"] = res;
            conn.Close();
            dr.Close();
            return res;


        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["type"] == null || Session["type"].ToString() != "User")
            {
                Response.Redirect("/Login.aspx");
            }
            if (Session["user"] != null)
            {
                var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = con.ConnectionString.ToString();

                SqlCommand cmd = new SqlCommand();
                SqlCommand cmd1 = new SqlCommand();

                cmd.Connection = conn;
                cmd1.Connection = conn;

                cmd.CommandType = System.Data.CommandType.Text;
                cmd1.CommandType = System.Data.CommandType.Text;

                cmd.CommandText = "select * from tbl_Information where Username='" + Session["user"].ToString() + "'";

                SqlDataReader dr;
                conn.Open();
                dr = cmd.ExecuteReader();
                dr.Read();
                name.Text = dr["Firstname"].ToString();

                family.Text = dr["Lastname"].ToString();
                father.Text = dr["Fathername"].ToString();
                DOB.Text = dr["DOB"].ToString();
                mellicode.Text = dr["mellicode"].ToString();

                profilepic.ImageUrl = "/admin/ProfilePicture/" + GetPersonalID(Session["user"].ToString()) + ".jpg";
                FLN.Text = dr["Firstname"].ToString() + ' ' + dr["Lastname"].ToString();

                SqlDataReader dr1;
                cmd1.CommandText = "select top 1 LastSSID from tbl_Information where ParentID='" + dr["personalID"].ToString() + "'";
                dr1 = cmd1.ExecuteReader();
                dr1.Read();
                SSIDFamily.Text = dr1["LastSSID"].ToString().Substring(0, 5);



                conn.Close();
                dr.Close();

            }


        }


        protected void vamShow_Click(object sender, EventArgs e)
        {
            vam.Visible = true;
            pardakhti.Visible = false;
            FamilyRow.Visible = false;

        }

        protected void Pardakhtishow_Click(object sender, EventArgs e)
        {

            pardakhti.Visible = true;
            vam.Visible = false;
            FamilyRow.Visible = false;

        }

        protected void familyShow_Click(object sender, EventArgs e)
        {
            pardakhti.Visible = false;
            vam.Visible = false;
            FamilyRow.Visible = true;
        }
    }
}