﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SandoghKayedi.user
{
    public partial class MellatVerify : System.Web.UI.Page
    {
        string RefId; string ResCode; string saleOrderId; string SaleReferenceId;
        string monthyear; string FamilyID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Form["RefId"] != null)
                RefId = Request.Form["RefId"].ToString();
            if (Request.Form["ResCode"] != null)
                ResCode = Request.Form["ResCode"].ToString();
            if (Request.Form["saleOrderId"] != null)
                saleOrderId = Request.Form["saleOrderId"].ToString();
            if (Request.Form["SaleReferenceId"] != null)
                SaleReferenceId = Request.Form["SaleReferenceId"].ToString();
            DbtblPardakhtiSelect();
            lblFamily.Text = FamilyFider(FamilyID);
            lblMonthyear.Text = monthyear;

            lblOrderID.Text = saleOrderId;
            lblRefrenceID.Text = SaleReferenceId;
            if (lblRefrenceID.Text == String.Empty)
                lblRefrenceID.BackColor = Color.Red;
            using (var webService = new ir.shaparak.bpm.PaymentGatewayImplService())
            {
                if (ResCode == "0")
                {
                    if (webService.bpVerifyRequest(publicdata.mellatTerminal, publicdata.mellatUser, publicdata.mellatPassword, long.Parse(saleOrderId), long.Parse(saleOrderId), long.Parse(SaleReferenceId)) == "0")
                    {
                        if (webService.bpSettleRequest(publicdata.mellatTerminal, publicdata.mellatUser, publicdata.mellatPassword, long.Parse(saleOrderId), long.Parse(saleOrderId), long.Parse(SaleReferenceId)) == "0")
                        {
                            //ok
                            DbtblGateWaySabt();

                            lblResponse.Text = "پرداخت شما با موفقیت انجام شد و به حساب شما منظور شد";
                            lblResponse.ForeColor = Color.Green;
                            DBtblPardakhtisabt();

                        }
                        else
                        {
                            //dar settle moshkel hast
                            webService.bpReversalRequest(publicdata.mellatTerminal, publicdata.mellatUser, publicdata.mellatPassword, long.Parse(saleOrderId), long.Parse(saleOrderId), long.Parse(SaleReferenceId));
                            lblResponse.Text = "در عملیات انتقال وجه مشکلی به وجود آمده است، اگر پول از حساب شما کم شده باشد، به حساب شما برگشت داده می شود";
                            lblResponse.ForeColor = Color.Red;
                        }
                    }
                    else
                    {
                        //dar verify moshkel hast
                        webService.bpReversalRequest(publicdata.mellatTerminal, publicdata.mellatUser, publicdata.mellatPassword, long.Parse(saleOrderId), long.Parse(saleOrderId), long.Parse(SaleReferenceId));
                        lblResponse.Text = "در عملیات بررسی پرداخت مشکلی به وجود آمده است، اگر پول از حساب شما کم شده باشد، به حساب شما برگشت داده می شود";
                        lblResponse.ForeColor = Color.Red;
                    }
                }
                else
                {
                    //pardakht ba khata
                    lblResponse.Text = "پرداخت با خطا همراه بوده است";
                    lblResponse.ForeColor = Color.Red;
                    admin.GatewayClass gc = new admin.GatewayClass();
                    lblResponse.Text = lblResponse.Text + " " + gc.DesribtionStatusCode(int.Parse(ResCode));
                }

            }
        }
        protected void DbtblGateWaySabt()
        {
            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.Text;

            cmd.CommandText = "Update tbl_Gateway set RefID='"+RefId+"' ,ResCode='"+ResCode+"',SaleOrderID='"+saleOrderId+"',SaleReferenceId='"+SaleReferenceId+"'  where OrderID='"+saleOrderId+"' ";
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        protected void DbtblPardakhtiSelect()
        {
            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.Text;

            cmd.CommandText = "select * from tbl_Gateway where ORderID='"+saleOrderId+"' ";
            SqlDataReader dr;
            conn.Open();
            dr=cmd.ExecuteReader();
            if(dr.Read())
            {
                FamilyID = dr["FamilyID"].ToString();
                monthyear = dr["MonthYear"].ToString();
            }
            conn.Close();
        }
        protected string FamilyFider(string familyID)
        {
            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.Text;

            cmd.CommandText = "select * from tbl_Information where PersonalID='" + familyID + "' ";
            SqlDataReader dr;
            string temp="";
            conn.Open();
            dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                temp = dr["Firstname"].ToString() +" " +dr["Lastname"].ToString();
            }
            conn.Close();
            return temp;
        }

        protected void DBtblPardakhtisabt()
        {
            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            System.Globalization.PersianCalendar pc = new System.Globalization.PersianCalendar();

           string date= pc.GetYear(DateTime.Now) + "/" + pc.GetMonth(DateTime.Now) + "/" + pc.GetDayOfMonth(DateTime.Now);

            cmd.CommandText = "SubmitPardakhtiFamily";
            cmd.Parameters.Add("@ParentID", SqlDbType.Int).Value = FamilyID;
            cmd.Parameters.Add("@date", SqlDbType.NVarChar).Value = monthyear;
            cmd.Parameters.Add("@PayType", SqlDbType.NVarChar).Value = "درگاه ملت";
            cmd.Parameters.Add("@Fishdate", SqlDbType.NVarChar).Value = date;
            cmd.Parameters.Add("@fish", SqlDbType.NVarChar).Value = SaleReferenceId;
            cmd.Parameters.Add("@tozihat", SqlDbType.NVarChar).Value = "";



            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

        }

    }
}