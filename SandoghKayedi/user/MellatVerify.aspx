﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ad.Master" AutoEventWireup="true" CodeBehind="MellatVerify.aspx.cs" Inherits="SandoghKayedi.user.MellatVerify" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                نتیجه پرداخت ملت     
            </header>
            <img src="/admin/img/dargah/mellatdargah.png" style="margin:0px auto" />
            <div class="panel-body">
                <div class="input-group m-bot15" style="width: 300px">
                    <span class="input-group-addon">خانواده</span>
                    <asp:Label CssClass="form-control blacktextbox" ForeColor="Black" runat="server" ID="lblFamily"></asp:Label>
                </div>
      
                <div class="input-group m-bot15" style="width: 300px">
                    <span class="input-group-addon">ماه و سال</span>
                    <asp:Label CssClass="form-control blacktextbox" ForeColor="Black" runat="server" ID="lblMonthyear"></asp:Label>
                </div>

                <div class="input-group m-bot15" style="width: 300px">
                    <span class="input-group-addon">شماره درخواست</span>
                    <asp:Label CssClass="form-control blacktextbox" ForeColor="Black" runat="server" ID="lblOrderID"></asp:Label>
                </div>
                <div class="input-group m-bot15" style="width: 300px">
                    <span class="input-group-addon">کد رهگیری</span>
                    <asp:Label CssClass="form-control blacktextbox" ForeColor="Black" runat="server" ID="lblRefrenceID"></asp:Label>
                </div>
                <div class="input-group m-bot15" style="width: 300px">
                    <span class="input-group-addon">نتیجه پرداخت</span>
                    <asp:Label CssClass="form-control blacktextbox" runat="server" ID="lblResponse"></asp:Label>
                </div>
            </div>
        </section>
    </div>


</asp:Content>
