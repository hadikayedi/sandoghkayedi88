﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Drawing;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using Telerik.Web.UI;


namespace RSTAS.user
{
    public partial class Information : System.Web.UI.Page
    {
        private string _personalId;
        protected int GetActiveUserCount()
        {
            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "select count(*)  from tbl_Information where PersonalID>0 and Active=1 and parentid=" + _personalId;

            SqlDataReader dr;
            conn.Open();
            dr = cmd.ExecuteReader();
            dr.Read();
            string a = dr[0].ToString();
            conn.Close();
            return int.Parse(a);



        }
        //protected int GetActiveFamilyCount()
        //{
        //    var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

        //    SqlConnection conn = new SqlConnection();
        //    conn.ConnectionString = con.ConnectionString.ToString();

        //    SqlCommand cmd = new SqlCommand();
        //    cmd.Connection = conn;
        //    cmd.CommandType = System.Data.CommandType.Text;
        //    cmd.CommandText = "select count(*)  from tbl_Information where PersonalID<0 and Active=1";

        //    SqlDataReader dr;
        //    conn.Open();
        //    dr = cmd.ExecuteReader();
        //    dr.Read();
        //    string a = dr[0].ToString();
        //    conn.Close();
        //    return int.Parse(a);



        //}
        protected int GetVamCount()
        {
            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "select count(*)  from vam where  ssidf=" + _personalId;

            SqlDataReader dr;
            conn.Open();
            dr = cmd.ExecuteReader();
            dr.Read();
            string a = dr[0].ToString();
            conn.Close();
            return int.Parse(a);



        }
        protected int GetdeActiveUserCount()
        {
            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "select count(*)  from tbl_Information where PersonalID>0 and Active=0 and parentid=" + _personalId;

            SqlDataReader dr;
            conn.Open();
            dr = cmd.ExecuteReader();
            dr.Read();
            string a = dr[0].ToString();
            conn.Close();
            return int.Parse(a);



        }
        //protected int GetdeActiveFamilyCount()
        //{
        //    var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

        //    SqlConnection conn = new SqlConnection();
        //    conn.ConnectionString = con.ConnectionString.ToString();

        //    SqlCommand cmd = new SqlCommand();
        //    cmd.Connection = conn;
        //    cmd.CommandType = System.Data.CommandType.Text;
        //    cmd.CommandText = "select count(*)  from tbl_Information where PersonalID<0 and Active=0";

        //    SqlDataReader dr;
        //    conn.Open();
        //    dr = cmd.ExecuteReader();
        //    dr.Read();
        //    string a = dr[0].ToString();
        //    conn.Close();
        //    return int.Parse(a);



        //}

        protected int GetMonthCount()
        {
            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;


            cmd.CommandText = "GetMonthCountToNow";
            PersianDateTime now = PersianDateTime.Now;
            string persianDate = now.ToString(PersianDateTimeFormat.Date);
            string lastdate = (persianDate.Substring(0, 7)).Replace("/", "");
            cmd.Parameters.Add("@date", SqlDbType.Int).Value = lastdate;

            SqlDataReader dr;
            conn.Open();
            dr = cmd.ExecuteReader();
            dr.Read();
            int temp = int.Parse(dr["MonthCountToNow"].ToString());
            conn.Close();
            return temp;

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["type"] == null || Session["type"].ToString() != "User")
            {
                Response.Redirect("/Login.aspx");
            }
            if (Session["personalId"] != null)
            {
                _personalId = Session["personalId"].ToString();
            }
            else
            {
                Response.Redirect("/Login.aspx");
            }

            ActiveUserCount.Text = GetActiveUserCount().ToString();
            //ActiveFamilyCount.Text = GetActiveFamilyCount().ToString();
            VamCount.Text = GetVamCount().ToString();

            FromBegin.Text = GetMonthCount().ToString();
            DeactiveUserCount.Text = GetdeActiveUserCount().ToString();
            //DeactiveFamilyCount.Text = GetdeActiveFamilyCount().ToString();

            SqlDataSource1.SelectCommand =
                $"SELECT *,Firstname+' '+Lastname as FLN,Lastmonth+' '+Lastyear as Lastmonthyear FROM InformationWithVamPardakhti2 where personalId={_personalId} or parentId={_personalId} order by Information_ID asc";
            informationGrid.DataSourceID = "SqlDataSource1";

            informationGrid.DataBind();
        }


        protected void informationGrid_OnItemDataBound(object sender, TreeListItemDataBoundEventArgs e)
        {
            if (e.Item is TreeListDataItem item)
            {
                string value = (string)DataBinder.Eval(item.DataItem, "Active");

                if (value != "فعال")
                {
                    item.BackColor = Color.Gray;


                }

            }
        }
    }
}