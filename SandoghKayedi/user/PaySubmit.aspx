﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ad.Master" AutoEventWireup="true" CodeBehind="PaySubmit.aspx.cs" Inherits="SandoghKayedi.user.PaySubmit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../script/PersianDatePicker.js"></script>
    <script src="../script/PersianDatePicker.min.js"></script>
    <link href="../Content/PersianDatePicker.min.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    

    <style>
        
div.rcbSlide .RadComboBoxDropDown .rcbScroll
       {
 		font-family: 'B Yekan';
         font-size:14px;
    }

    </style>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:cnnstring %>' ProviderName="System.Data.SqlClient" SelectCommand="SELECT *,Firstname+' '+Lastname as FLN FROM [tbl_Information] where ParentID IS NULL and Active=1"></asp:SqlDataSource>
   <%-- <asp:UpdateProgress runat="server" AssociatedUpdatePanelID="UpdatePanel1" >
        <ProgressTemplate>
             <div class="loading" align="center">
    Loading. Please wait.<br />
    <br />
    <img src="https://www.aspsnippets.com/demos/loader.gif" alt="" />
</div>
        </ProgressTemplate>
    </asp:UpdateProgress>--%>
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" ChildrenAsTriggers="true" runat="server">
      
        <ContentTemplate>
            
             <div class="col-lg-6"    >
                        <section class="panel">
                            <header class="panel-heading">
                                                  پرداخت     
                            </header>
                                <div class="panel-body">
                       <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="FamilySubmit" ForeColor="Red" runat="server" DisplayMode="BulletList" ShowMessageBox="False" EnableClientScript="True" Font-Strikeout="False" />


            <div class="input-group m-bot15" style="width:300px" >
                                <span class="input-group-addon">خانواده</span>
                <telerik:RadComboBox    CssClass="form-control blacktextbox" Font-Size="14px"  Font-Names="B yekan" EmptyMessage="انتخاب نمایید" AllowCustomText="true" Filter="Contains"  AutoPostBack="true" ID="Rad" runat="server" DataSourceID="SqlDataSource1" OnSelectedIndexChanged="Rad_SelectedIndexChanged" DataTextField="FLN" DataValueField="PersonalID" ></telerik:RadComboBox>
                 </div>


            <div class="input-group m-bot15" style="width:250px" >
                                <span class="input-group-addon">مبلغ (ریال)</span>
            <asp:TextBox ReadOnly="true" BackColor="White" ID="Cost" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
                 </div>

            <div class="input-group m-bot15" style="width:350px" >
                                <span class="input-group-addon">ماه</span>
                <telerik:RadComboBox Enabled="false"  BackColor="White" CssClass="form-control blacktextbox" ForeColor="Black" Font-Size="14px"  Font-Names="B yekan"   ID="ComboMonth" runat="server"  >
                    <Items>
                        <telerik:RadComboBoxItem runat="server" Text="فروردین" Value="01"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="اردیبهشت" Value="02"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="خرداد" Value="03"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="تیر" Value="04"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="مرداد" Value="05"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="شهریور" Value="06"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="مهر" Value="07"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="آبان" Value="08"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="آذر" Value="09"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="دی" Value="10"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="بهمن" Value="11"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="اسفند" Value="12"></telerik:RadComboBoxItem>
                    </Items>
                </telerik:RadComboBox>
                <span class="input-group-addon">سال</span>
            <asp:TextBox ReadOnly="true"  BackColor="White" ID="TextLastyear" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
            </div>
                                    <br />
                                    <div class="input-group m-bot15" style="width:300px" >
                                <span class="input-group-addon">نوع پرداخت</span>
                <telerik:RadComboBox  CssClass="form-control blacktextbox " Enabled="false" AutoPostBack="true" Font-Size="14px"  Font-Names="B yekan" OnSelectedIndexChanged="Paytype_SelectedIndexChanged"      ID="Paytype" runat="server"  >
                    <Items>
                     <telerik:RadComboBoxItem runat="server" Text="از طریق درگاه اینترنتی" Value="درگاه"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="پرداخت اینترنتی" Value="پرداخت اینترنتی"></telerik:RadComboBoxItem>
                        <telerik:RadComboBoxItem runat="server" Text="پرداخت نقدی" Value="پرداخت نقدی"></telerik:RadComboBoxItem>
                    </Items>
                </telerik:RadComboBox>
                 </div>
                                    <div class="input-group m-bot15" runat="server" id="dargahbox" visible="true"  style="width:250px;" >
                                <span class="input-group-addon">درگاه</span>
<asp:RadioButtonList ID="dargahlist" runat="server">
         <asp:ListItem Text='<img src="/admin/img/dargah/mellatdargah.png" alt="img1" />' Value="mellat" Selected="True" />
</asp:RadioButtonList>         
                                        <br />
                                        <asp:Label runat="server" ID="dargahmessage"></asp:Label>
                 </div>

                                    <div class="input-group m-bot15" runat="server" id="datebox" visible="false"  style="width:250px;" >
                                <span class="input-group-addon">تاریخ</span>
                                         
            <asp:TextBox  ID="datepicker"  CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator DisplayMode="BulletList" Display="None" ControlToValidate="datepicker" ID="RequiredFieldValidator1" ValidationGroup="FamilySubmit" runat="server" ForeColor="Red" ErrorMessage="تاریخ را وارد نمایید"></asp:RequiredFieldValidator>
                                       
                 </div>

                                    <div class="input-group m-bot15" runat="server" id="fishbox" visible="false" style="width:250px" >
                                <span class="input-group-addon">شماره فیش</span>
            <asp:TextBox  ID="fish"  CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator DisplayMode="BulletList" Display="None" ControlToValidate="fish" ID="RequiredFieldValidator2" ValidationGroup="FamilySubmit" runat="server" ForeColor="Red" ErrorMessage="شماره فیش را وارد نمایید"></asp:RequiredFieldValidator>

                 </div>
                                    <div class="input-group m-bot15" runat="server" id="tozihatbox" style="width:250px" >
                                <span class="input-group-addon">توضیحات</span>
            <asp:TextBox  ID="desc" TextMode="MultiLine" CssClass="form-control blacktextbox" ForeColor="Black" Font-Names="B yekan"  runat="server"  ></asp:TextBox>
                 </div>

                                <asp:Button ID="SubmitFamilydargah" runat="server" Enabled="false" CssClass="btn btn-info" Text="پرداخت" ValidationGroup="FamilySubmit" OnClick="SubmitFamily_Click" />
                 <asp:Button ID="SubmitFamily" runat="server" Enabled="false" CssClass="btn btn-info" Text="ثبت" ValidationGroup="FamilySubmit" OnClick="SubmitFamily_Click" Visible="false" />

                                       <br />

                                    <div runat="server" id="GreenMessagebox" visible="false" style="background-color:lightgreen;margin-top:20px;width:300px;margin:0px auto;height:100px;text-align:center;border-radius:20px">
                                        شماره فیش مورد نظر با موفقیت ثبت شد
                                        <br />
                                        <br />
                                        <br />

                                <asp:Button ID="SubmitNewFish" runat="server" CssClass="btn btn-info" Text="ثبت فیش جدید" OnClick="SubmitNewFish_Click"  />

            </div>
                                    </section>
                 </div>
            
            <%--  --%>
            <%--  --%>
            <%--  --%>
            <%--  --%>
            <%--  --%>
            <%--  --%>
            <%--  --%>
            <%--  --%>

            <div class="col-lg-6"   >
                        <section class="panel">
                            <header class="panel-heading">
                                                  اعضای خانواده     
                            </header>
                                <div class="panel-body">
                                    <telerik:RadGrid runat="server" OnDataBound="Radgrid_DataBound" AlternatingItemStyle-Font-Names="B yekan" HeaderStyle-Font-Names="b yekan" ItemStyle-Font-Names="B yekan" CellSpacing="-1" DataSourceID="SqlDataSource2" GridLines="Both" GroupPanelPosition="Top" ID="Radgrid" OnItemCommand="Radgrid_ItemCommand" >
                                        <MasterTableView DataSourceID="SqlDataSource2" AutoGenerateColumns="False">
                                            <Columns>
<%--                                                <telerik:GridImageColumn DataImageUrlFields="PersonalID" DataImageUrlFormatString="/admin/ProfilePicture/{0}.jpg" AlternateText=" " ImageHeight="32" ImageWidth="32"></telerik:GridImageColumn>--%>
                                                <telerik:GridBoundColumn DataField="PersonalID" HeaderText="شماره عضویت" SortExpression="PersonalID" UniqueName="PersonalID" DataType="System.Int64" FilterControlAltText="Filter PersonalID column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="FLN" HeaderText="نام و نام خانوادگی" SortExpression="FLN" UniqueName="FLN" FilterControlAltText="Filter FLN column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="Loanturn" HeaderText="نوبت وام" SortExpression="Loanturn" UniqueName="Loanturn" DataType="System.Int64" FilterControlAltText="Filter Loanturn column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="SumOzviat" HeaderText="مجموع حق عضویت" SortExpression="SumOzviat" UniqueName="SumOzviat" DataType="System.Int64" FilterControlAltText="Filter SumOzviat column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="SumVam" HeaderText="مجموع وام دریافتی" SortExpression="SumVam" UniqueName="SumVam" DataType="System.Int64" FilterControlAltText="Filter SumVam column"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="Active" HeaderText="فعال" SortExpression="Active" UniqueName="Active" DataType="System.Int64" FilterControlAltText="Filter Active column"></telerik:GridBoundColumn>
                                                                                                                    

                                            </Columns>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                    <asp:SqlDataSource runat="server" ID="SqlDataSource2" ConnectionString='<%$ ConnectionStrings:cnnstring %>' ProviderName="System.Data.SqlClient" SelectCommand="SELECT *,Firstname+' '+Lastname as FLN FROM [InformationWithVamPardakhti] WHERE ([ParentID] = @ParentID) "  >
                                        

                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="Rad" PropertyName="SelectedValue" Name="ParentID" Type="Int64"></asp:ControlParameter>
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                        </section>
                </div>
           
            
        </ContentTemplate>
         <Triggers>
<asp:PostBackTrigger ControlID="SubmitFamilydargah" >
            </asp:PostBackTrigger>

       </Triggers>
        
    </asp:UpdatePanel>
</asp:Content>
