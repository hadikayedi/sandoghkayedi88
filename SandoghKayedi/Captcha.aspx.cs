﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Drawing.Drawing2D;

namespace SandoghKayedi
{
    public partial class Captcha : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Bitmap bmp=new Bitmap(Server.MapPath("~\\images\\Captcha.jpg"));
            MemoryStream memstream=new MemoryStream();
            int width=bmp.Width;
            int height=bmp.Height;
            string FontFamily = "Arial";
            string text = Request.Cookies["Captcha"]["value"];
            Bitmap bitmap = new Bitmap(bmp, new Size(width, height));
            Graphics g = Graphics.FromImage(bitmap);
            g.SmoothingMode = SmoothingMode.AntiAlias;
            int xCopy = width - 150;
            int yCopy = height - 50;
            Rectangle rect;
            Font font;
            int newfontsize = 45;
            font = new Font(FontFamily, newfontsize, FontStyle.Italic);
            rect = new Rectangle(xCopy, yCopy, 0, 0);
            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Near;
            format.LineAlignment = StringAlignment.Near;
            GraphicsPath patch = new GraphicsPath();
            patch.AddString(text, font.FontFamily, (int)font.Style, font.Size, rect, format);
            HatchBrush hatchbrush = new HatchBrush(HatchStyle.LargeConfetti, Color.FromName("white"), Color.FromName("black"));
            g.FillPath(hatchbrush, patch);
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentType = "image/jpeg";
            bitmap.Save(memstream, ImageFormat.Jpeg);
            hatchbrush.Dispose();
            g.Dispose();
            memstream.WriteTo(HttpContext.Current.Response.OutputStream);
            bitmap.Dispose();


        }
    }
}