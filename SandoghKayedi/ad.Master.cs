﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;
namespace SandoghKayedi
{
    public partial class ad : System.Web.UI.MasterPage
    {
        protected string GetPersonalID(string Username)
        {

            var con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = con.ConnectionString.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "select * from tbl_Information where Username='" + Username + "'";

            SqlDataReader dr;
            conn.Open();
            dr = cmd.ExecuteReader();
            dr.Read();
            return dr["PersonalID"].ToString();


        }

        protected void Page_Load(object sender, EventArgs e)
        {


            if (Session["user"] != null)
            {
                if (Session["type"].ToString() == "Admin")
                {

                    khoshamad.Text = Session["FLn"].ToString();
                    profilepic.ImageUrl = "/admin/ProfilePicture/" + GetPersonalID(Session["user"].ToString()) + ".jpg";
                    UserKartabl.Visible = false;
                    Kartabl.Visible = true;
                    BaseInformation.Visible = true;
                    Bank.Visible = true;
                    Report.Visible = true;


                }
                else if (Session["type"].ToString() == "User")
                {

                    UserKartabl.Visible = true;
                    Kartabl.Visible = false;
                    BaseInformation.Visible = false;
                    Bank.Visible = false;
                    Report.Visible = false;

                    khoshamad.Text = Session["FLn"].ToString();
                    string PicAddress = "/admin/ProfilePicture/" + GetPersonalID(Session["user"].ToString()) + ".jpg";
                    if (File.Exists("PicAddress"))
                        profilepic.ImageUrl = PicAddress;
                    else
                        profilepic.ImageUrl = "/admin/ProfilePicture/nobody.png";




                }
                else
                    Response.Redirect("/Login.aspx");
            }
            else
                Response.Redirect("/Login.aspx");

        }
    }
}