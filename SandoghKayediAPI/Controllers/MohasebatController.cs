﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.VisualBasic;
using System.Web;
namespace SandoghKayediAPI.Controllers
{

    public class MohasebatController : ApiController
    {
        public static string Base64Encode(string plainText)
        {
            try
            {
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
                return System.Convert.ToBase64String(plainTextBytes);
            }
            catch
            {
                return null;
            }
        }
        public static string Base64Decode(string base64EncodedData)
        {
            try
            {
                var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
                return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            }
            catch
            {
                return null;
            }
        }
        private SqlConnection CS(string Par)
        {

            SqlConnection con;
            if (Base64Decode(Par) == "a")
                con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["cnnstring"].ConnectionString);
            else
                con = null;
            string url3 = HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath;

            return con;
        }
        [HttpGet]
        public HttpResponseMessage GetAllInformation(string Par)
        {
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(CS(Par).ConnectionString.ToString()))
            {

                string querystring = "SELECT * from Tbl_Information ";
                SqlCommand cmd = new SqlCommand(querystring, conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds, "Data");

            }
            return Request.CreateResponse(HttpStatusCode.OK, ds);
        }
        [HttpGet]
        //[Route("test")]
        //[Route("api/test2")]

        public HttpResponseMessage GetPayeRow(string year, string month, string Par)
        {
            DataSet ds = new DataSet();
            string x;
            //get call rul
            string url2 = Request.RequestUri.ToString();
            string baseUrl = Url.Request.RequestUri.GetComponents(
    UriComponents.SchemeAndServer, UriFormat.Unescaped);
            //get request website url
            string url3 = HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath;
            // return (user.firstName.ToString() + " <> " + user.lastName);
            

            string monthyear = year + month;
            using (SqlConnection conn = new SqlConnection(CS(Par).ConnectionString.ToString()))
            {

                string querystring = "SELECT * from PayeNew where fr<='" + monthyear + "' and '" + monthyear + "'<t";
                SqlCommand cmd = new SqlCommand(querystring, conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(ds, "Data");

            }
            //mohasebat(Par);
            //          foreach (DataRow myRow in ds.Tables[0].Rows)
            //{
            //    x=  myRow["haghozviat"].ToString(); 
            //}
            return Request.CreateResponse(HttpStatusCode.OK, ds);
        }


        private string GetFirstMonth(string Par)
        {
            string output = null;

            using (SqlConnection conn = new SqlConnection(CS(Par).ConnectionString.ToString()))
            {
                string querystring = "SELECT top 1 * from payenew order by fr Asc ";
                SqlCommand cmd = new SqlCommand(querystring, conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    output = dr["fr"].ToString();

                }
                conn.Close();
            }
            return output;
        }
        private string GetLastMonth(string Par)
        {
            string output = null;

            using (SqlConnection conn = new SqlConnection(CS(Par).ConnectionString.ToString()))
            {
                string querystring = "SELECT top 1 * from payenew order by fr Desc ";
                SqlCommand cmd = new SqlCommand(querystring, conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    output = dr["antrack"].ToString();

                }
                conn.Close();
            }
            return output;
        }
        private Paye GetPayeInformation(string Date, string Par)
        {
            Paye Output = new Paye();
            using (SqlConnection conn = new SqlConnection(CS(Par).ConnectionString.ToString()))
            {

                string querystring = "SELECT * from payenew where fr<='" + Date + "' and antrack>='" + Date + "' ";
                SqlCommand cmd = new SqlCommand(querystring, conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    Output.Date = int.Parse(Date);
                    Output.HagheOzviat = dr["haghozviat"].ToString();
                    Output.Marhale = dr["idvam"].ToString();
                    Output.Vam = dr["vam"].ToString();
                    Output.TedadVam = int.Parse(dr["tedad"].ToString());
                    if (int.Parse(Date) > int.Parse(dr["t"].ToString()) && int.Parse(Date) <= int.Parse(dr["antrack"].ToString()))
                        Output.IsAntrack = true;
                    else
                        Output.IsAntrack = false;
                }
                conn.Close();
            }
            return Output;
        }
        private int CountBetween2Month(string first, string last)
        {
            int tempfirstyear = int.Parse(first.Left(4));
            int tempfirstmoth = int.Parse(first.Right(2));
            int templastyear = int.Parse(last.Left(4));
            int templastmonth = int.Parse(last.Right(2));

            int count;

            if (templastyear == tempfirstyear)
                count = (templastmonth - tempfirstmoth);
            else if (templastyear > tempfirstyear)
            {
                count = (templastyear - tempfirstyear) * 12;
                count = count + (templastmonth - tempfirstmoth);
            }
            else
                count = 0;

            return count;


        }
        private string AddMonth(string Date, int add)
        {
            string year = Date.Left(4);

            string month = Date.Right(2);
            month = (int.Parse(month) + add).ToString();
            if (int.Parse(month) > 12)
            {
                int tempyear = int.Parse(month) / 12;
                int tempmonth = int.Parse(month) - tempyear * 12;

                year = (int.Parse(year) + tempyear).ToString();
                month = tempmonth.ToString();
            }

            if (month.Length == 1)
                month = "0" + month;



            return year.ToString() + month.ToString();
        }
        private int TedadeNafarat(string Date, string Par)
        {
            int count;
            using (SqlConnection conn = new SqlConnection(CS(Par).ConnectionString.ToString()))
            {

                string querystring = "SELECT count(*) from Tbl_Information where active='1' and PersonalID>0 and EnterDate<='" + Date + "' ";
                SqlCommand cmd = new SqlCommand(querystring, conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                count = int.Parse(dr[0].ToString());
                conn.Close();
            }
            return count;
        }
        private int VamMonthCount(string Date, string Par)
        {

            int count;
            using (SqlConnection conn = new SqlConnection(CS(Par).ConnectionString.ToString()))
            {

                string querystring = "SELECT count(*) from vam where monthyear='" + Date + "' ";
                SqlCommand cmd = new SqlCommand(querystring, conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                count = int.Parse(dr[0].ToString());
                conn.Close();
            }
            return count;


        }
        public Mohasebat mohasebat(string Par)
        {
            Mohasebat OutPut = new Mohasebat();
            string start = GetFirstMonth(Par);
            int count;

            for (int i = 1; i <= CountBetween2Month(GetFirstMonth(Par), GetLastMonth(Par)); i++)
            {

                Paye Payeinformation = GetPayeInformation(start, Par);

                MohasebatDetail md = new MohasebatDetail();
                md.Date = Payeinformation.Date;
                count = TedadeNafarat(start, Par);

                md.SumOzviat = (int.Parse(Payeinformation.HagheOzviat) * count);
                md.Bazpardakht = OutPut.mohasebatdetail.Where(p => p.LastMonth >= md.Date).Sum(q => q.Bazpardakhtmonth);

                md.SumMonth = OutPut.mohasebatdetail.Select(p => p.MandeMonth).LastOrDefault() + md.SumOzviat + md.Bazpardakht;
                md.sumVamCount = OutPut.mohasebatdetail.Sum(p => p.VamTedad);

                md.VamTedad = md.SumMonth / int.Parse(Payeinformation.Vam);
                if (md.sumVamCount + md.VamTedad > int.Parse(Payeinformation.Marhale) * count)

                    md.VamTedad = int.Parse(Payeinformation.Marhale) * count - md.sumVamCount;



                md.Bazpardakhtmonth = (md.VamTedad * (int.Parse(Payeinformation.Vam) / Payeinformation.TedadVam));

                if (Payeinformation.Vam == "-1")
                {
                    md.VamTedad = 0;
                    //md.Bazpardakhtmonth = 0;
                    md.LastMonth = 0;

                }
                else
                {
                    md.LastMonth = int.Parse(AddMonth(md.Date.ToString(), Payeinformation.TedadVam));

                }
                //var x = OutPut.mohasebatdetail.Where(p => p.LastMonth >= md.Date);

                if (Payeinformation.Vam == "-1")
                {
                    md.VamTedad = 0;
                    md.Bazpardakhtmonth = 0;
                    md.LastMonth = 0;

                }
                else
                {
                    md.LastMonth = int.Parse(AddMonth(md.Date.ToString(), Payeinformation.TedadVam));

                }
                md.MandeMonth = md.SumMonth - (md.VamTedad * int.Parse(Payeinformation.Vam));


                int tempTedadVamLastmonth = OutPut.mohasebatdetail.Sum(p => p.VamTedad);
                for (int j = tempTedadVamLastmonth; j <= md.VamTedad + tempTedadVamLastmonth; j++)
                {

                    if (j != tempTedadVamLastmonth)
                    {
                        if (j > count)
                        {
                            TblInformation ti = new TblInformation();

                            ti.LoanTurn = j - count * (int.Parse(Payeinformation.Marhale) - 1);

                            md.Information.Add(ti);
                        }
                        else
                        {
                            TblInformation ti = new TblInformation();

                            ti.LoanTurn = j;

                            md.Information.Add(ti);
                        }
                    }
                }


                start = AddMonth(start, 1);
                OutPut.mohasebatdetail.Add(md);
            }



            return OutPut;

        }


    }
}
