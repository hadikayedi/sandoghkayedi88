﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace SandoghKayediAPI
{

    public class Paye 
    {
       

        public int Date { get; set; }
        public string Marhale { get; set; }
        public bool IsAntrack { get; set; }
        public string HagheOzviat { get; set; }
        public string Vam { get; set; }
        public int TedadVam { get; set; }
    }

    public class Mohasebat
    {
        public List<MohasebatDetail> mohasebatdetail { get; set; }
        public Mohasebat()
        {
            mohasebatdetail = new List<MohasebatDetail>();
        }
    }
    public class MohasebatDetail
    {
        public int Date { get; set; }
        public int SumOzviat { get; set; }
        public int VamTedad { get; set; }
        public int Bazpardakht { get; set; }
        public int Bazpardakhtmonth { get; set; }
        public int LastMonth { get; set; }
        public int SumMonth { get; set; }
        public int MandeMonth { get; set; }
        public int sumVamCount { get; set; }
        public string marhale { get; set; }
        public List<TblInformation> Information { get; set; }

        public MohasebatDetail()
        {
            Information = new List<TblInformation>();
        }
    }
    public class TblInformation
    {
        public int LoanTurn { get; set; }
    }
    
    
}